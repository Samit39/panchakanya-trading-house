<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

/**
 * Description of my_controller
 *
 * @author Administrator
 */
class MY_Controller extends MX_Controller {
    public $data = array();
    public $permission_name;
    function __construct() {
        parent::__construct();
        if (version_compare(CI_VERSION, '2.1.0', '<')) {
            $this->load->library('security');
        }
        //
        if($this->session->has_userdata('is_logged_in'))
        {
            /*
            if($this->permission_name!='')
            {
                if($this->check_permission($this->permission_name))
                {

                }
                else
                {
                    $this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
                    redirect('dashboard');
                }
            }
            */
        }
        else
        {
            redirect('login');
        }
        //no title for public and admin template
        //$this->template->write('title', '', TRUE);
        //$this->template->write('content', '', TRUE);
    }

    public function check_permission($permissionname)
    {
        //Getting permission id from permission_name
      
        $permission_id = '';
        $this->db->select('t1.*');
        $this->db->from('tbl_permissions t1');
        $this->db->where('t1.name',$permissionname);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $permission_id = $query->result()[0]->id;
        }
        else
        {
            return false;
        }
        //
        $group_id = '';
        if($permission_id!='')
        {
            $user_data = $this->session->all_userdata();
            $group_id = $user_data['group_id'];
            if($group_id==1){
                return true;
            }
            if($group_id!='')
            {
                //Checking permission with group_id and permission_id
                $this->db->select('t1.*');
                $this->db->from('tbl_group_permission t1');
                $this->db->where('t1.group_id',$group_id);
                $this->db->where('t1.permission_id',$permission_id);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        //
    }

    public function set_log($moduleId='',$moduleTitle='',$action='')
    {
        $set_log_flag = 1;
        if($set_log_flag == 1)
        {
            $user_data = $this->session->all_userdata();
            $moduleName = $this->router->fetch_class();
            //
            $data['moduleName'] = $moduleName;
            $data['moduleId'] = ($moduleId=='') ? 1 : $moduleId; //Set 1 for clicked
            $data['moduleTitle'] = ($moduleTitle == '') ? "Listing Page Viewed On ".$moduleName : $moduleTitle;
            $data['action'] = ($action=='') ? 1 : $action;
            $data['dateTime'] = date("Y-m-d H:i:s");
            $data['ip'] = get_ip_address();
            $data['user_id'] = $user_data['user_id'];
            //
            $this->db->insert('tbl_logreport',$data);
        }
    }

    public function form($id='', $module, $pageview = '_form') {

        $this->$module->__construct();
        // pr($module);

        //$this->data['body'] = BACKENDFOLDER.'/'.$module.'/_form';
        $this->data['body'] = $module . '/'.$pageview;
        //        print_r ($this->data['body']);die;
        if ($_POST) {
            $this->data[$module] = (object) $_POST;
          
        } elseif ($id != '') {
            $this->data[$module] = $this->$module->get(1, array('id' => $id));
          
        } else {
            $this->data[$module] = $this->$module;
        }
        if ($id == '')
            $this->data['sub_module_name'] = ucwords($module) . ' Add';
        else
            $this->data['sub_module_name'] = ucwords($module) . ' Edit';
      
        $this->render();
    }

    public function sendNativeMailInitial_demo($params)
    {
        if(!empty($params))
        {
            $this->load->library('email');

            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            // $config['mailpath'] = 'E:\xampp\sendmail\sendmail.ini';
            $config['charset'] = 'iso-8859-1';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);

            $this->email->from($params['from'], $params['fromname']);
            $this->email->to($params['to']);
            // $this->email->cc('another@another-example.com');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject($params['subject']);
            $this->email->message($params['message']);
            // $this->email->message("Hello");

           $kei= $this->email->send();
            /*
            //
            $to = $params['to'];
            $subject = $params['subject'];
            $message = $params['message'];
            // To send HTML mail, the Content-type header must be set
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            // Additional headers
            // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
            // $headers[] = 'From: Birthday Reminder <birthday@example.com>';
            $headers[] = 'From:'.'<'.$params['from'].'>';
            // $headers[] = 'Cc: birthdayarchive@example.com';
            // $headers[] = 'Bcc: birthdaycheck@example.com';

            // Mail it
            mail($to, $subject, $message, implode("\r\n", $headers));
            //
            */
        }
        else
        {
            echo "Parameter Is Not Sent While Mail Sending";
            die();
        }
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */