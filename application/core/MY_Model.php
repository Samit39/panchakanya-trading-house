<?php

class MY_Model extends CI_Model {

    /*
    protected $table;
    protected $log_table = 'activity_log';
    protected $global_config = '';
    protected $created_timestamp = false;
    protected $created_by = false;
    protected $updated_timestamp = false;
    protected $updated_by = false;
    */

    public function __construct() {
        parent::__construct();
    }

    public function sendNativeMailInitial($params)
    {
        if(!empty($params))
        {
            $this->load->library('email');

            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            // $config['mailpath'] = 'E:\xampp\sendmail\sendmail.ini';
            $config['charset'] = 'iso-8859-1';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);

            $this->email->from($params['from'], $params['fromname']);
            $this->email->to($params['to']);
            // $this->email->cc('another@another-example.com');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject($params['subject']);
            $this->email->message($params['message']);
            // $this->email->message("Hello");

           $kei= $this->email->send();
            /*
            //
            $to = $params['to'];
            $subject = $params['subject'];
            $message = $params['message'];
            // To send HTML mail, the Content-type header must be set
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            // Additional headers
            // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
            // $headers[] = 'From: Birthday Reminder <birthday@example.com>';
            $headers[] = 'From:'.'<'.$params['from'].'>';
            // $headers[] = 'Cc: birthdayarchive@example.com';
            // $headers[] = 'Bcc: birthdaycheck@example.com';

            // Mail it
            mail($to, $subject, $message, implode("\r\n", $headers));
            //
            */
        }
        else
        {
            echo "Parameter Is Not Sent While Mail Sending";
            die();
        }
    }
}