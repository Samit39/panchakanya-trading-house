<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('dashboard_model', 'dashboard');
    }

	public function index()
	{
		$data = array();
		$data['breadcrumbs_title'] = "Dashboard";
		$data['content'] = "dashboard";
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}
}
