<div class="row">
    <div class="col-lg-12">
        <?php
        $user_data = $this->session->all_userdata();
        $accessible_module_array = explode(",", $user_data['accessible_module']);
        ?>
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <a href="<?php echo base_url().'daybooks' ?>" class="card card--withShadow paymentAccount">
            <div class="paymentAccount__icon">
                <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                <i class="fa fa-book" aria-hidden="true"></i>
            </div>
            <span class="paymentAccount__number">Daybooks</span>
            <!-- <span class="paymentAccount__sum">$2,156.78</span> -->
            <!-- <div class="paymentAccount__footer">Current month charged: $10,200.00</div> -->
        </a>
    </div>
    <div class="col-lg-4">
        <a href="<?php echo base_url().'payments' ?>" class="card card--withShadow paymentAccount">
            <div class="paymentAccount__icon">
                <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                <i class="fa fa-money" aria-hidden="true"></i>
            </div>
            <span class="paymentAccount__number">Payments</span>
            <!-- <span class="paymentAccount__sum">$2,156.78</span> -->
            <!-- <div class="paymentAccount__footer">Current month charged: $10,200.00</div> -->
        </a>
    </div>
    <div class="col-lg-4">
        <a href="<?php echo base_url().'statements' ?>" class="card card--withShadow paymentAccount">
            <div class="paymentAccount__icon">
                <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                <i class="fa fa-files-o" aria-hidden="true"></i>
            </div>
            <span class="paymentAccount__number">Statements</span>
            <!-- <span class="paymentAccount__sum">$2,156.78</span> -->
            <!-- <div class="paymentAccount__footer">Current month charged: $10,200.00</div> -->
        </a>
    </div>
</div>