<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Controller {

	public function __construct() {
        parent::__construct();
        //
		$this->permission_name = "Groups";
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
		//
        $this->load->model('groups_model', 'groups');
        
    }

	public function index()
	{
		$this->set_log();   
		$this->load->library('form_validation');
		$data = array();
		$data['breadcrumbs_title'] = "Groups And Permissions";
		$data['content'] = '_list';
		$data['group_list'] = $this->groups->getAllGroups();
		// pr($data['group_list']);
		// die();
		$data['addJs'] = array('assets/js/group.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$group_id = segment(3);
		$post = $_POST;
		if($group_id!='')
		{
			
			$data['group_data'] = $this->groups->get_detail_by_id($group_id);
			$data['group_id'] = $group_id;
			
		}
		if(!empty($post))
		{
			$this->form_validation->set_rules($this->groups->rules());

		if ($this->form_validation->run() == true)
		{

			if($group_id!='')
			{
			
				//EDIT ko lagi
				$this->groups->update_group($group_id,$post);
				//
				$this->set_log($group_id,"Edited Group - ".$post['name'],6);
				//
				$this->session->set_flashdata('success', 'Group Successfully Edited.');
				redirect('groups');
			}
			else
			{

				//Add ko lagi
				
				$post['created'] = date("Y-m-d h:i:s");
				$inserted_group_id = $this->groups->insert_new_group($post);
				//
				$this->set_log($inserted_group_id,"Created Group - ".$post['name'],5);
				//
				$this->session->set_flashdata('success', 'Group Successfully Added.');
				redirect('groups');
			}
		}
	}	
		$data['breadcrumbs_title'] = "Add Group";
		$data['content'] = '_form';
		$data['addJs'] = array('assets/js/group.js');

		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
		

	}
	public function change_status()
    {
        $group_id = segment(3);
        if($group_id !='')
        {
            $this->groups->change_status_of_groups($group_id);
            $this->session->set_flashdata('success', 'Group Status Successfully Changed.');
        }
        else
        {
            $this->session->set_flashdata('error', 'Parameter error while Changing status.');
        }
        $this->set_log($group_id,"Status Changed Of Groupid - ".$group_id,17);
        redirect('groups');
    }


	public function delete()
	{
		$group_id = segment(3);
		if($group_id !='')
		{
			$this->groups->delete_group($group_id);
			$this->session->set_flashdata('success', 'Group Successfully Deleted.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		$this->set_log($group_id,"Deleted Group",7);
		redirect('groups');
	}

	public function getAllUserListOfThisGroup()
	{
		$group_id=$_POST['groupid'];
		$users_result = $this->groups->getAllUsersInThisGroup($group_id);
    	echo json_encode(array(
    		'status' => true,
    		'message' => 'User List Retreived Successfully',
    		'result' => $users_result
    	));
	}
}
