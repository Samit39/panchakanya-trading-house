<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
               <?php echo $this->session->flashdata('error'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php
            // pr($group_data);
            if(isset($group_id) && $group_id != '')
            {
                $form_submit_url = base_url().'groups/create/'.$group_id;
            }
            else
            {
                $form_submit_url = base_url().'groups/create';
            } 
            ?>
            <form id="group-formsubmit" autocomplete="off" action="<?php echo $form_submit_url; ?>" method="POST">
                <div class="row col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Group Name:*</label>
                            <input type="text" class="form-control" placeholder="Group Name" name="name" value="<?php echo(isset($group_data) && $group_data->name!='') ? $group_data->name : '' ?>">
                            <?php echo form_error("name"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description:*</label>
                            <input type="text" class="form-control" placeholder="Description" name="description" value="<?php echo(isset($group_data) && $group_data->description!='') ? $group_data->description : '' ?>">
                             <?php echo form_error("description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="active">
                                <option value="1" <?php echo(isset($group_data) && $group_data->active==1) ? 'selected' : '' ?> >Active</option>
                                <option value="0" <?php echo(isset($group_data) && $group_data->active==0) ? 'selected' : '' ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-lg-12">
                        <div class="form-group">
                            <input name="bodgroup" type="checkbox" value="1" <?php echo(isset($group_data) && $group_data->bodgroup==1) ? 'checked' : '' ?> > <label>Is BOD Group</label>
                        </div>
                    </div> -->
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input id="submit" type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'groups'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>