<div class="row">

    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
           <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <a href="<?php echo base_url().'groups/create' ?>" class="btn btn-rounded btn-primary mr-2 mb-3"><i class="fa fa-plus" aria-hidden="true"></i> Add Group</a>
        <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
        <div class="mb-5">
            <table class="table table-hover display" id="groups_table">
                <thead>
                    <tr>
                        <th >SN</th>
                        <th >Group_Name</th>
                        <th >Description</th>
                        <th >Number_of_users</th>
                        <th >Status</th>
                        <th >Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($group_list))
                    {
                    $i = 1;
                    foreach ($group_list as $value) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->name; ?></td>
                        <td><?php echo $value->description; ?></td>
                        <td><?php echo ($value->users == 0) ? "N/A" : $value->users; ?></td>
                         <td><?php
                          if ( $value->active === '1') {
                            echo 'Active';
                        }else
                        {
                            echo'Inactive';
                        } ?></td>

                        <td>
                            <div class="btn-group mb-2 table-action-icon-group" aria-label="" role="group">
                                <!-- <a class="btn btn-success table-action-icon"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                <a href="<?php echo base_url() . 'groups/create/' . $value->id; ?>" class="btn btn-sm btn-warning table-action-icon" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="#myModal" class="btn btn-sm btn-success table-action-icon" title="View All Members" id="view_all_members" data-toggle="modal" data-id="<?php echo $value->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <?php
                                if($value->active==0)
                                {
                                    $change_status_title = "Change to Active";
                                ?>
                                <a href="<?php echo base_url() ?>groups/change_status/<?php echo $value->id ?>"class="btn btn-sm btn-green table-action-icon"  onclick="return changeStatusToActive();" title="<?php echo $change_status_title; ?>"><i class="fa fa-circle-o" aria-hidden="true"></i></a>
                                <?php
                                }
                                else
                                {
                                    $change_status_title = "Change to Inactive";
                                ?>
                                <a href="<?php echo base_url() ?>groups/change_status/<?php echo $value->id ?>"class="btn btn-sm btn-green table-action-icon"  onclick="return changeStatusToInActive();" title="<?php echo $change_status_title; ?>"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                <?php
                                }
                                ?>
                                <a href="<?php echo base_url() . 'groups/delete/' . $value->id; ?>" class="btn btn-sm btn-danger table-action-icon"  onclick="return deletechecked();" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    } //for each loop
                    } //if condition
                    ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
<!--FOR MODAL -->
<!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">List Of Users</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <div class="fetched-data"></div> <!-- //Here Will show the Data -->
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<!--FOR MODAL -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var ajaxUrl = $('#baseUrl').val()+'groups/getAllUserListOfThisGroup';
            var groupid = $(e.relatedTarget).data('id');
            var thead= "<table class='table table-hover display'> <thead> <tr> <th>SN</th> <th>Full Name</th> <th>Status</th> </tr> </thead> <tbody>";
            var tbody = "";
            var tfoot = "</tbody> </table>";
            var activetext = "";
            $.ajax({
                url: ajaxUrl,
                data: {
                    groupid:groupid
                },
                type: 'post',
                success: function (data) {
                    // data = JSON.parse(data);
                    data = JSON.parse(data);

                    $.each(data.result, function(i, field){
                        k=i++;
                        if(field.active==1)
                        {
                            activetext = "Active";
                        }
                        else
                        {
                            activetext = "Inactive";
                        }
                        tbody = tbody + "<tr><td>"+parseInt(i)+"</td><td>"+field.firstname+" "+field.middlename+" "+field.lastname+"</td><td>"+activetext+"</td></tr>";
                        // $('#branch_id').append('<option value="'+field.id+'">'+field.title+'</option>');
                    }); //each ends here
                    $('.fetched-data').html(thead+tbody+tfoot);
                }//Success ends here
            }); //Ajax ends here
        });
    });
</script>
<script>
     function changeStatusToActive()
    {
        if(confirm("Do you want to activate this group?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function changeStatusToInActive()
    {
        if(confirm("Do you want to deactivate this group?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
       function deletechecked()
    {
        if(confirm("Are You Sure You Want To Delete This Group?"))
        {
            return true;
        }
        else
        {
            return false;
        }
   }
(function($) {
"use strict";
$(function () {
$('#groups_table').DataTable({
responsive:false
});
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>
