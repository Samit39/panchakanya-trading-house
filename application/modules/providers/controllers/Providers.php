<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Providers extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Providers";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('providers_model', 'providers');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Item Providers Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->providers->getAllInformation();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$provider_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->providers->getAllUsersList();
		//
		if($provider_id!='')
		{
			$data['providers_data'] = $this->providers->get_detail_by_id($provider_id);
			$data['provider_id'] = $provider_id;
			// pr($data['providers_data']);
			// echo $data['providers_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->providers->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($provider_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_providers
				$tbl_providers_data['title'] = $post['title'];
				$tbl_providers_data['description'] = $post['description'];
				$tbl_providers_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_providers_data['status'] = 1;
				$tbl_providers_data['updated_by'] = $user_data['user_id'];
				//
				$this->providers->update_information($provider_id,$tbl_providers_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('providers');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_providers
				$tbl_providers_data['title'] = $post['title'];
				$tbl_providers_data['description'] = $post['description'];
				$tbl_providers_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_providers_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_providers_data['status'] = 1;
				$tbl_providers_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_providers_id = $this->providers->insert_new_information($tbl_providers_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('providers');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/providers.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$providers_id = segment(3);
		if($providers_id !='')
		{
			$this->providers->delete_providers($providers_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('providers');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "providers Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->providers->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$providers_id = segment(3);
		if($providers_id !='')
		{
			$this->providers->recycle_providers($providers_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In providers.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('providers/recycle_bin_list');
	}
}
