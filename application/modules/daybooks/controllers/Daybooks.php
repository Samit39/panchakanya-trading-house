<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daybooks extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Daybooks";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('daybooks_model', 'daybooks');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Daybooks Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->daybooks->getAllInformation();
		// pr($data['information_list']);
		// die();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$daybook_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		$data['items_list'] = $this->daybooks->getAllActiveItemsList();
		$data['item_provides_list'] = $this->daybooks->getAllActiveItemProvidersList();
		$data['customers_list'] = $this->daybooks->getAllActiveCustomersList();
		//
		if($daybook_id!='')
		{
			$data['daybooks_data'] = $this->daybooks->get_detail_by_id($daybook_id);
			$data['daybook_id'] = $daybook_id;
			// pr($data['daybooks_data']);
			// echo $data['daybooks_data']->name;
			// die();
		}
		if(!empty($post))
		{
			// pr($post);
			// die();
			// $this->form_validation->set_rules($this->daybooks->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($daybook_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_daybooks
				$tbl_daybooks_data['date'] = $post['date'];
				$tbl_daybooks_data['item_id'] = $post['item_select'];
				if($post['khata_select'] == "Kharid Khata")
				{
					$tbl_daybooks_data['item_provider_id'] = $post['item_provider_select'];
					$tbl_daybooks_data['customer_id'] = "";
				}
				else if($post['khata_select'] == "Bikri Khata")
				{
					$tbl_daybooks_data['item_provider_id'] = "";
					$tbl_daybooks_data['customer_id'] = $post['customer_select'];
				}
				$tbl_daybooks_data['ch_number'] = $post['ch_number'];
				$tbl_daybooks_data['price'] = $post['price'];
				$tbl_daybooks_data['description'] = $post['description'];
				//
				$tbl_daybooks_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_daybooks_data['status'] = 1;
				$tbl_daybooks_data['updated_by'] = $user_data['user_id'];
				//
				$this->daybooks->update_information($daybook_id,$tbl_daybooks_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('daybooks');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_daybooks
				$tbl_daybooks_data['date'] = $post['date'];
				$tbl_daybooks_data['item_id'] = $post['item_select'];
				if($post['khata_select'] == "Kharid Khata")
				{
					$tbl_daybooks_data['item_provider_id'] = $post['item_provider_select'];
					$tbl_daybooks_data['customer_id'] = "";
				}
				else if($post['khata_select'] == "Bikri Khata")
				{
					$tbl_daybooks_data['item_provider_id'] = "";
					$tbl_daybooks_data['customer_id'] = $post['customer_select'];
				}
				$tbl_daybooks_data['ch_number'] = $post['ch_number'];
				$tbl_daybooks_data['price'] = $post['price'];
				$tbl_daybooks_data['description'] = $post['description'];
				//
				$tbl_daybooks_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_daybooks_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_daybooks_data['status'] = 1;
				$tbl_daybooks_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_daybooks_id = $this->daybooks->insert_new_information($tbl_daybooks_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('daybooks');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		$data['addCss'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.css'
		);
		$data['addJs'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.js',
			'assets/js/daybooks.js'
		);
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$daybooks_id = segment(3);
		if($daybooks_id !='')
		{
			$this->daybooks->delete_daybooks($daybooks_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('daybooks');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "daybooks Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->daybooks->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$daybooks_id = segment(3);
		if($daybooks_id !='')
		{
			$this->daybooks->recycle_daybooks($daybooks_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In daybooks.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('daybooks/recycle_bin_list');
	}
}
