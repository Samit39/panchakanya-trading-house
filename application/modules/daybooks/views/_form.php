<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
                <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <?php
            // pr($daybooks_data);
            if(isset($daybook_id) && $daybook_id != '')
            {
                $form_submit_url = base_url().'daybooks/create/'.$daybook_id;
            }
            else
            {
                $form_submit_url = base_url().'daybooks/create';
            } 
            ?>
            <?php //pr($daybooks_data); echo $daybooks_data[0]->name; ?>
            <form id="daybooks_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Date: *</label>
                            <input type="text" class="form-control" placeholder="Select Date" id="date" name="date" value="<?php echo(isset($daybooks_data) && $daybooks_data[0]->date!='') ? $daybooks_data[0]->date : '' ?>" autocomplete="off">
                            <?php echo form_error("date"); ?>
                        </div>
                        <div class="form-group">
                            <label>Select Item: *</label>
                            <select class="form-control" id ="item_select" name="item_select">
                                <option value="">--Select Item--</option>
                            <?php
                            if(isset($items_list) && !empty($items_list))
                            {
                                foreach ($items_list as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($daybooks_data) && $daybooks_data[0]->item_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("item_select"); ?>
                        </div>
                        <div class="form-group">
                            <label>Select Khata: *</label>
                            <select class="form-control" id ="khata_select" name="khata_select">
                                <option value="">--Select Khata--</option>
                                <option value="Kharid Khata" <?php echo (isset($daybooks_data) && $daybooks_data[0]->item_provider_id != 0) ? "selected" : '' ?>>Kharid Khata</option>
                                <option value="Bikri Khata" <?php echo (isset($daybooks_data) && $daybooks_data[0]->customer_id != 0) ? "selected" : '' ?>>Bikri Khata</option>
                            </select>
                            <?php echo form_error("khata_select"); ?>
                        </div>
                        <div class="form-group" id="item_provider_select_div">
                            <label>Select Item Provider: *</label>
                            <select class="form-control" id ="item_provider_select" name="item_provider_select">
                                <option value="">--Select Item Provider--</option>
                            <?php
                            if(isset($item_provides_list) && !empty($item_provides_list))
                            {
                                foreach ($item_provides_list as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($daybooks_data) && $daybooks_data[0]->item_provider_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("item_provider_select"); ?>
                        </div>
                        <div class="form-group" id="customer_select_div">
                            <label>Select Customer: *</label>
                            <select class="form-control" id ="customer_select" name="customer_select">
                                <option value="">--Select Customer--</option>
                            <?php
                            if(isset($customers_list) && !empty($customers_list))
                            {
                                foreach ($customers_list as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($daybooks_data) && $daybooks_data[0]->customer_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("customer_select"); ?>
                        </div>
                        <div class="form-group">
                            <label>Unique Number (Chalani Number):</label>
                            <input type="text" class="form-control" placeholder="Unique Number (Chalani Number)" id="ch_number" name="ch_number" value="<?php echo(isset($daybooks_data) && $daybooks_data[0]->ch_number!='') ? $daybooks_data[0]->ch_number : '' ?>">
                            <?php echo form_error("ch_number"); ?>
                        </div>
                        <div class="form-group">
                            <label>Price: *</label>
                            <input type="number" min="1" class="form-control" placeholder="Price" id="price" name="price" value="<?php echo(isset($daybooks_data) && $daybooks_data[0]->price!='') ? $daybooks_data[0]->price : '' ?>">
                            <?php echo form_error("price"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description Notes (if any):</label>
                            <input type="text" class="form-control" placeholder="Description Notes (if any)" id="description" name="description" value="<?php echo(isset($daybooks_data) && $daybooks_data[0]->description!='') ? $daybooks_data[0]->description : '' ?>">
                            <?php echo form_error("description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'daybooks'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>