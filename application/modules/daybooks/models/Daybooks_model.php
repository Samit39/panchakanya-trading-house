<?php

class Daybooks_model extends MY_Model {

    public function __construct() {
        parent::__construct();
      
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
            
 
        );

        return $array;
    }
    */
    public function getAllInactiveInformation()
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_daybooks t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',0);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }

	public function getAllInformation()
	{
		$final_result = array();
		$this->db->select('t1.*,t3.title as item_title,t4.title as item_provider_name,t5.title as customer_name');
		$this->db->from('tbl_daybooks t1');
		$this->db->join('tbl_items t3','t3.id = t1.item_id');
		$this->db->join('tbl_providers t4','t4.id = t1.item_provider_id','left');
		$this->db->join('tbl_customers t5','t5.id = t1.customer_id','left');
		$this->db->order_by('t1.date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function insert_new_information($data)
	{
		$this->db->insert('tbl_daybooks', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function get_detail_by_id($id)
	{	
		$this->db->select('t1.*');
		$this->db->from('tbl_daybooks t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function update_information($daybook_id,$data)
	{
		$this->db->where('id', $daybook_id);
		$this->db->update('tbl_daybooks', $data);
	}

	public function delete_daybooks($daybook_id)
	{
		$data['status'] = 0;
		$this->db->where('id', $daybook_id);
		$this->db->update('tbl_daybooks', $data);
	}

	public function recycle_daybooks($daybook_id)
	{
		$data['status'] = 1;
		$this->db->where('id', $daybook_id);
		$this->db->update('tbl_daybooks', $data);
	}
	//
	public function getAllActiveItemsList()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_items t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getAllActiveItemProvidersList()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_providers t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getAllActiveCustomersList()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_customers t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
}