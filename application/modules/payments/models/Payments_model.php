<?php

class Payments_model extends MY_Model {

    public function __construct() {
        parent::__construct();
      
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
            
 
        );

        return $array;
    }
    */
    public function getAllInactiveInformation()
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_payments t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',0);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }

	public function getAllInformation()
	{
		$result_1 = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_payments t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result_1 = $query->result();
			$i = 0;
			foreach ($result_1 as $value) 
			{
				if($value->account_type!='' && $value->account_id!='')
				{
					if($value->account_type == "Item Providers")
					{
						$account_name = $this->getTitleFromItemProvidersById($value->account_id);
					}
					else if($value->account_type = "Customers")
					{
						$account_name = $this->getTitleFromCustomersById($value->account_id);
					}
					else
					{
						$account_name = "Account Type Undetected";
					}
				}
				else
				{
					$account_name = "UNDEFINED";
				}
				$result_1[$i]->account_name = $account_name;
				$i++;
			}
			return $result_1;
		}
		else
		{
			return false;
		}
	}

	public function insert_new_information($data)
	{
		$this->db->insert('tbl_payments', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function get_detail_by_id($id)
	{	
		$this->db->select('t1.*');
		$this->db->from('tbl_payments t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function update_information($item_id,$data)
	{
		$this->db->where('id', $item_id);
		$this->db->update('tbl_payments', $data);
	}

	public function delete_payments($item_id)
	{
		$data['status'] = 0;
		$this->db->where('id', $item_id);
		$this->db->update('tbl_payments', $data);
	}

	public function recycle_payments($item_id)
	{
		$data['status'] = 1;
		$this->db->where('id', $item_id);
		$this->db->update('tbl_payments', $data);
	}
	//
	public function getActiveItemProviders()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_providers t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getActiveCustomers()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_customers t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getTitleFromItemProvidersById($id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_providers t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0]->title;
		}
		else
		{
			return "NULL";
		}
	}

	public function getTitleFromCustomersById($id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_customers t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0]->title;
		}
		else
		{
			return "NULL";
		}
	}
}