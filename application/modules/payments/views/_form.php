<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
                <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
                <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <?php
            // pr($payments_data);
            if(isset($payment_id) && $payment_id != '')
            {
                $form_submit_url = base_url().'payments/create/'.$payment_id;
            }
            else
            {
                $form_submit_url = base_url().'payments/create';
            } 
            ?>
            <?php //pr($payments_data); echo $payments_data[0]->name; ?>
            <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
            <form id="payments_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Date: *</label>
                            <input type="text" class="form-control" placeholder="Select Date" id="date" name="date" value="<?php echo(isset($payments_data) && $payments_data[0]->date!='') ? $payments_data[0]->date : '' ?>" autocomplete="off">
                            <?php echo form_error("date"); ?>
                        </div>
                        <div class="form-group">
                            <label>Select Transaction Type: *</label>
                            <select class="form-control" id ="trans_select" name="trans_select">
                                <option value="">--Select Transaction Type--</option>
                                <option value="Dr" <?php echo (isset($payments_data) && $payments_data[0]->trans_type=='Dr') ? "selected" : '' ?> >Debit</option>
                                <option value="Cr" <?php echo (isset($payments_data) && $payments_data[0]->trans_type== 'Cr') ? "selected" : '' ?> >Credit</option>
                            </select>
                            <?php echo form_error("trans_select"); ?>
                        </div>
                        <div class="form-group">
                            <label>Select Account Type: *</label>
                            <select class="form-control" id ="account_type_select" name="account_type_select">
                                <option value="">--Select Account Type--</option>
                                <option value="Item Providers" <?php echo (isset($payments_data) && $payments_data[0]->account_type=='Item Providers') ? "selected" : '' ?> >Item Providers</option>
                                <option value="Customers" <?php echo (isset($payments_data) && $payments_data[0]->account_type== 'Customers') ? "selected" : '' ?> >Customers</option>
                            </select>
                            <?php echo form_error("account_type_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Account Holder's Name: *</label>
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo(isset($payments_data) && $payments_data[0]->account_id!='') ? $payments_data[0]->account_id : '' ?>">
                            <select class="form-control" id ="account_holders_name_select" name="account_holders_name_select">
                                <option value="">--Select Account Holders Name--</option>
                            </select>
                            <?php echo form_error("account_holders_name_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Payment Amount: *</label>
                            <input type="number" min="1" class="form-control" placeholder="Payment Amount" id="payment_amount" name="payment_amount" value="<?php echo(isset($payments_data) && $payments_data[0]->payment_amount!='') ? $payments_data[0]->payment_amount : '' ?>">
                            <?php echo form_error("payment_amount"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description:</label>
                            <input type="text" class="form-control" placeholder="Description" id="description" name="description" value="<?php echo(isset($payments_data) && $payments_data[0]->description!='') ? $payments_data[0]->description : '' ?>">
                            <?php echo form_error("description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'payments'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>