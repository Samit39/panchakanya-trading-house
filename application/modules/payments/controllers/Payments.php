<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Payments";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('payments_model', 'payments');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Payments Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->payments->getAllInformation();
		// pr($data['information_list']);
		// die();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$payment_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->payments->getAllUsersList();
		//
		if($payment_id!='')
		{
			$data['payments_data'] = $this->payments->get_detail_by_id($payment_id);
			$data['payment_id'] = $payment_id;
			// pr($data['payments_data']);
			// echo $data['payments_data']->name;
			// die();
		}
		if(!empty($post))
		{
			// pr($post);
			// die();
			// $this->form_validation->set_rules($this->payments->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($payment_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_payments
				$tbl_payments_data['date'] = $post['date'];
				$tbl_payments_data['trans_type'] = $post['trans_select'];
				$tbl_payments_data['account_type'] = $post['account_type_select'];
				$tbl_payments_data['account_id'] = $post['account_holders_name_select'];
				$tbl_payments_data['payment_amount'] = $post['payment_amount'];
				$tbl_payments_data['description'] = $post['description'];
				//
				$tbl_payments_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_payments_data['status'] = 1;
				$tbl_payments_data['updated_by'] = $user_data['user_id'];
				//
				$this->payments->update_information($payment_id,$tbl_payments_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('payments');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_payments
				$tbl_payments_data['date'] = $post['date'];
				$tbl_payments_data['trans_type'] = $post['trans_select'];
				$tbl_payments_data['account_type'] = $post['account_type_select'];
				$tbl_payments_data['account_id'] = $post['account_holders_name_select'];
				$tbl_payments_data['payment_amount'] = $post['payment_amount'];
				$tbl_payments_data['description'] = $post['description'];
				//
				$tbl_payments_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_payments_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_payments_data['status'] = 1;
				$tbl_payments_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_payments_id = $this->payments->insert_new_information($tbl_payments_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('payments');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		$data['addCss'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.css'
		);
		$data['addJs'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.js',
			'assets/js/payments.js'
		);
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$payments_id = segment(3);
		if($payments_id !='')
		{
			$this->payments->delete_payments($payments_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('payments');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "payments Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->payments->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$payments_id = segment(3);
		if($payments_id !='')
		{
			$this->payments->recycle_payments($payments_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In payments.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('payments/recycle_bin_list');
	}

	//
	public function getAccountNamesByAccountType()
	{
		$account_type=$_POST['account_type'];
		if($account_type!='')
		{
			// $account_type = "Customers";
			if($account_type == "Item Providers")
			{
				$result = $this->payments->getActiveItemProviders();
			}
			else if($account_type == "Customers")
			{
				$result = $this->payments->getActiveCustomers();
			}
			else
			{
				echo json_encode(array(
		    		'status' => false,
		    		'message' => 'Account Type Cannot Be Empty'
		    	));
			}
			echo json_encode(array(
	    		'status' => true,
	    		'message' => 'Account Names Retreived Successfully.',
	    		'result' => $result
	    	));
		}
		else
		{
			echo json_encode(array(
	    		'status' => false,
	    		'message' => 'Error! POST Fields Empty.'
	    	));	
		}
	}
}
