<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <!-- <h5 class="text-black"><strong>Basic Responsive Table</strong></h5> -->
        <a href="<?php echo base_url() . 'users/create' ?>" class="btn btn-rounded btn-primary mr-2 mb-3"><i class="fa fa-plus" aria-hidden="true"></i> Add User</a>
        <div class="mb-5">
            <table class="table table-hover display" id="groups_table">
                <thead>
                    <tr>
                        <th >SN</th>
                        <th >Full Name</th>
                        <th >Email</th>
                        <th >Username</th>
                        <th >Address</th>
                        <th >Phone</th>
                         <th >Status</th>
                        <th >Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
if (isset($user_list)) {
    $i = 1;
    foreach ($user_list as $value) {
        if($value->id!=1)
        {
        ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                          <td><?php echo $value->firstname.' '.$value->middlename.' '.$value->lastname;?></td>

                        <td><?php echo $value->email; ?></td>
                         <td><?php echo $value->username; ?></td>
                        <td><?php echo $value->address ?></td>
                        <td><?php echo $value->phone ?></td>
                          <td><?php echo ($value->active == 0) ? "Inactive" : "Active"; ?></td>
                        <td>
                            <div class="btn-group mb-2 table-action-icon-group" aria-label="" role="group">
                                <!-- <a class="btn btn-success table-action-icon"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                <a href="<?php echo base_url() ?>users/create/<?php echo $value->id ?>" class="btn btn-sm btn-warning table-action-icon" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <?php
                                if($value->active==0)
                                {
                                    $change_status_title = "Change to Active";
                                ?>
                                <a href="<?php echo base_url() ?>users/change_status/<?php echo $value->id ?>"class="btn btn-sm btn-green table-action-icon"  onclick="return changeStatusToActive();" title="<?php echo $change_status_title; ?>"><i class="fa fa-circle-o" aria-hidden="true"></i></a>
                                <?php
                                }
                                else
                                {
                                    $change_status_title = "Change to Inactive";
                                ?>
                                <a href="<?php echo base_url() ?>users/change_status/<?php echo $value->id ?>"class="btn btn-sm btn-green table-action-icon"  onclick="return changeStatusToInActive();" title="<?php echo $change_status_title; ?>"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                <?php
                                }
                                ?>
                                <!-- <a href="<?php echo base_url() ?>users/change_status/<?php echo $value->id ?>"class="btn btn-success table-action-icon"  onclick="return changeStatuschecked();" title="<?php echo $change_status_title; ?>">
                                    <?php if($value->active == 1) { ?>
                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                    <?php }
                                    else{ ?>
                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                    <?php } ?>
                                </a> -->
                                <a href="<?php echo base_url() ?>users/delete/<?php echo $value->id ?>"class="btn btn-sm btn-danger table-action-icon"  onclick="return deletechecked();" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
        $i++;
    }
    } //for each loop
} //if condition
?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
<script>

    function changeStatusToActive()
    {
        if(confirm("Do you want to activate this user?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function changeStatusToInActive()
    {
        if(confirm("Do you want to deactivate this user?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
     function deletechecked()
    {
        if(confirm("Are You Sure You Want To Delete This User?"))
        {
            return true;
        }
        else
        {
            return false;
        }
   }
(function($) {
"use strict";
$(function () {
$('#groups_table').DataTable({
    responsive : true
// responsive: {
//     details:false
// }
});
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>
