<?php

class Users_model extends MY_Model
{
    protected $table = 'tbl_users';

    public function rules($id = '')
    {
        $array = array(

            array(
                'field' => 'groups_id',
                'label' => 'Role',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim',
            ),
            array(
                'field' => 'phone',
                'label' => 'Mobile Number',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'firstname',
                'label' => 'First Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'middlename',
                'label' => 'Middle Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'lastname',
                'label' => 'Last Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ),

        );

        return $array;
    }

    public function getAllUsers()
    {
        $this->db->select('t1.*');
        $this->db->from('tbl_users t1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function save($data, $condition = null, $return_id = true, $table = '')
    {

        if ($condition) {
            if ($this->db->field_exists('status', $this->table)) {
                //
            }
        } else {
            if ($this->session->userdata('role') == 'Editor') {
                if ($this->db->field_exists('status', $this->table)) {
                    $data['status'] = 'pending';
                }
            } else {
                if ($this->db->field_exists('status', $this->table)) {
                    $data['status'] = 'active';
                }
            }
        }
        if ($condition) {
            if ($this->updated_timestamp) {
                $data['updated_on'] = time();
            }

            if ($this->updated_by) {
                $data['updated_by'] = get_userdata('user_id') ? get_userdata('user_id') : null;
            }

            $return = $this->db->update($this->table, $data, $condition);
        } else {
            if ($this->created_timestamp) {
                $data['created_on'] = time();
            }

            if ($this->created_by) {
                $data['created_by'] = get_userdata('user_id') ? get_userdata('user_id') : null;
            }

            if ($this->updated_timestamp) {
                $data['updated_on'] = time();
            }

            if ($this->updated_by) {
                $data['updated_by'] = get_userdata('user_id') ? get_userdata('user_id') : null;
            }

            $return = $this->db->insert($this->table, $data);
        }
        if ($return) {
            $return = true;
            if ($return_id && !$condition) {
                $return = $this->db->insert_id();
            }
        } else {
            $return = false;
        }
        return $return;
    }

    public function validate_token($token)
    {
        $this->db->where('usercode', $token);
        $result = $this->db->get('tbl_users');
        if ($result->num_rows > 0) {
            return false;
        } else {
            return true;
        }

    }

    public function getUser($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('tbl_users')->row();
    }

    public function check_if_username_already_exists($username)
    {
        $this->db->select('t1.*');
        $this->db->from('tbl_users t1');
        $this->db->where('t1.username',$username);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    public function check_if_email_already_exists($email)
    {
        $this->db->select('t1.*');
        $this->db->from('tbl_users t1');
        $this->db->where('t1.email',$email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    public function delete_user($user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->delete('tbl_users');
    }

    public function change_status_of_user($user_id)
    {
        $this->db->select('t1.*');
        $this->db->from('tbl_users t1');
        $this->db->where('t1.id',$user_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            $user_current_status = $query->result()[0]->active;
            if($user_current_status==0)
            {
                $data['active'] = 1;
            }
            else
            {
                $data['active'] = 0;
            }
            $this->db->where('id', $user_id);
            $this->db->update('tbl_users', $data);
        } 
        else 
        {
            return false;
        }
    }

}
