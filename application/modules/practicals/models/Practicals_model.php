<?php

class Practicals_model extends MY_Model {

    public function __construct() {
        parent::__construct();
      
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
            
 
        );

        return $array;
    }
    */
    public function getAllInactiveInformation()
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_practical t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',0);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }

	public function getAllInformation()
	{
		$final_result = array();
		$this->db->select('t1.*,t2.title');
		$this->db->from('tbl_practical t1');
		$this->db->join('tbl_labs t2','t2.id=t1.lab_id');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getAllInformationByLabID($lab_id)
	{
		$this->db->select('t1.*,t2.title');
		$this->db->from('tbl_practical t1');
		$this->db->join('tbl_labs t2','t2.id=t1.lab_id');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',1);
		$this->db->where('t1.lab_id',$lab_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function insert_new_information($data)
	{
		$this->db->insert('tbl_practical', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function get_detail_by_id($id)
	{	
		$this->db->select('t1.*');
		$this->db->from('tbl_practical t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function update_information($practical_id,$data)
	{
		$this->db->where('id', $practical_id);
		$this->db->update('tbl_practical', $data);
	}

	public function delete_stocks($practical_id)
	{
		$data['status'] = 0;
		$this->db->where('id', $practical_id);
		$this->db->update('tbl_practical', $data);
	}

	public function recycle_labs($practical_id)
	{
		$data['status'] = 1;
		$this->db->where('id', $practical_id);
		$this->db->update('tbl_practical', $data);
	}

	public function getAllActiveLabs()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_labs t1');
		$this->db->where('t1.status',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
}