<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
        <div class="">
            <a href="<?php echo base_url().'practicals/create' ?>" class="btn btn-rounded btn-primary mr-2 mb-3 pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Information</a>
        </div>
        <hr style="margin-top: 58px !important;">
        <?php $form_submit_url = base_url().'practicals/search' ?>
        <div>
            <form id="practicals_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Lab for Practical Listing: *</label>
                            <select class="form-control" id ="lab_select" name="lab_select" required>
                                <option value="">--Select Lab--</option>
                            <?php
                            if(isset($labs_result) && !empty($labs_result))
                            {
                                foreach ($labs_result as $value) 
                                {
                                ?>
                                    <option <?php echo (isset($lab_id) && $lab_id == $value->id) ? "selected" : '' ;?> value="<?php echo $value->id; ?>"><?php echo $value->title;?></option>
                                <?php
                                } //for each ends
                            } //if ends
                            ?>
                            </select>
                            <?php echo form_error("lab_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input class="btn btn-rounded btn-primary mr-2 mb-3 pull-right" type="submit" name="submit" value="Search">
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <?php if(isset($information_list) && !empty($information_list)) { ?>
        <div class="mb-5 table-responsive" style="margin-top: 55px;">
            <table class="table table-hover display" id="practicals_table">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Date</th>
                        <th>Day</th>
                        <th>Lab Name</th>
                        <th>Name Of Practical</th>
                        <th>Teacher Name</th>
                        <th>Group</th>
                        <th>Class</th>
                        <th>Section</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($information_list) && !empty($information_list))
                    {
                    $i = 1;
                    foreach ($information_list as $value) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->practical_date; ?></td>
                        <td><?php echo $value->practical_day; ?></td>
                        <td><?php echo $value->title; ?></td>
                        <td><?php echo $value->practical_name; ?></td>
                        <td><?php echo $value->practical_teacher; ?></td>
                        <td><?php echo $value->practical_group; ?></td>
                        <td><?php echo $value->practical_class; ?></td>
                        <td><?php echo $value->practical_section; ?></td>
                        <td>
                            <div class="btn-group mb-2 table-action-icon-group" aria-label="" role="group">
                                <!-- <a class="btn btn-success table-action-icon"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                <a href="<?php echo base_url() . 'practicals/create/' . $value->id; ?>" class="btn btn-success table-action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url() . 'practicals/delete/' . $value->id; ?>" class="btn btn-success table-action-icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    } //for each loop
                    } //if condition
                    ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
        <?php  } ?>
    </div>
</div>
<script>
(function($) {
"use strict";
$(function () {
    var top_message = "The information in this table is copyright to Braincoders Technologies.";
    var file_title = "Practicals Information List";
    $('#practicals_table').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'csv',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'pdf',
                messageTop: top_message,
                title: file_title
            },
            {
                extend: 'print',
                messageTop: top_message,
                title: file_title
            }
        ]
    });
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#lab_select").select2();
    });
</script>