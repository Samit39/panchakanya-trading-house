<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_report extends MY_Controller {

	public function __construct() {
        parent::__construct();
        //
		$this->permission_name = "Log Report";
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
		//
        $this->load->model('log_report_model', 'log_report');
    }

	public function index()
	{
		$this->set_log();
		$data = array();
		$data['breadcrumbs_title'] = "Log Report";
		$data['content'] = '_list';
		$data['log_report_list'] = $this->log_report->getAllLogReports();
		// pr($data['log_report_list']);
		// die();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}
}
