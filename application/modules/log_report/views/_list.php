<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <div class="mb-5">
            <table class="table table-hover display" id="log_report_table">
                <thead>
                    <tr>
                        <th >SN</th>
                        <th >Module_Name</th>
                        <th >Module_ID</th>
                        <th >Module_Title</th>
                        <th >Action</th>
                        <th >Date/Time</th>
                        <th >IP</th>
                        <th >User Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($log_report_list))
                    {
                    $i = 1;
                    foreach ($log_report_list as $value) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->moduleName; ?></td>
                        <td><?php echo $value->moduleId; ?></td>
                        <td><?php echo $value->moduleTitle; ?></td>
                        <td><?php echo getActionName($value->action); ?></td>
                        <td><?php echo $value->dateTime; ?></td>
                        <td><?php echo $value->ip; ?></td>
                        <td><?php echo $value->username; ?></td>
                    </tr>
                    <?php
                    $i++;
                    } //for each loop
                    } //if condition
                    ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Date</th>
                    <th>Salary</th>
                </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
<?php
function getActionName($action_id)
{
  if($action_id==1)
  {
    return "Clicked";
  }
  else if($action_id==5)
  {
    return "Created";
  }
  else if($action_id==6)
  {
    return "Edited";
  }
  else if($action_id==7)
  {
    return "Deleted";
  }
  else if($action_id==8)
  {
    return "Logged In";
  }
  else if($action_id==9)
  {
    return "Logged Out";
  }
  else if($action_id==10)
  {
    return "Viewed";
  }
  else if($action_id==11)
  {
    return "Document Downloaded";
  }
  else if($action_id==12)
  {
    return "All Document Versions Downloaded";
  }
  else if($action_id==13)
  {
    return "Other";
  }
  else if($action_id==14)
  {
    return "Password Changed";
  }
  else if($action_id==15)
  {
    return "Failed Login";
  }
  else if($action_id==16)
  {
    return "Forgot Password";
  }
  else if($action_id==17)
  {
    return "Status Changed";
  }
  else
  {
    return "Void";
  }
}
?>
<script>
(function($) {
"use strict";
$(function () {
$('#log_report_table').DataTable({
responsive: true
});
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>
