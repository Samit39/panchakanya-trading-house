<?php

class My_account_model extends MY_Model {

    // public $table = 'groups';
    // public $id = '',$name = '',$description='';

    public function __construct() {
        parent::__construct();
      
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
             array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'trim|required',
            ),

 
        );

        return $array;
    }
    */
    public function checkIfThisPasswordIsValid($password)
    {
    	$user_data = $this->session->all_userdata();
		$user_id = $user_data['user_id'];
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('t1.id',$user_id);
		$this->db->where('t1.password',md5($password));
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
    }

    public function update_password($user_id,$new_password)
    {
    	$data['password'] = md5($new_password);
    	$data['pwd_change_on'] = date("Y-m-d H:i:s");
    	$this->db->where('id', $user_id);
		$this->db->update('tbl_users', $data);
    }

    public function update_email($user_id,$new_email)
    {
    	$data['email'] = $new_email;
    	$this->db->where('id', $user_id);
		$this->db->update('tbl_users', $data);
		$this->session->set_userdata('email',$new_email);
    }

    //FROM HERE OLD
	public function getAllGroups()
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			// return $query->result();
			foreach ($query->result() as $value) {
				$value->users = $this->getUsersCount($value->id);
				array_push($final_result,$value);
			}
			return $final_result;
		}
		else
		{
			return false;
		}
	}

	public function getUsersCount($group_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_users t1');
		$this->db->where('t1.groups_id',$group_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}

	public function insert_new_group($data)
	{
		$this->db->insert('tbl_groups', $data);
	}

	public function get_detail_by_id($group_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$this->db->where('t1.id',$group_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function update_group($group_id,$data)
	{
		$this->db->where('id', $group_id);
		$this->db->update('tbl_groups', $data);
	}

	public function delete_group($group_id)
	{
		$this->db->where('id', $group_id);
		$this->db->delete('tbl_groups');
	}
}