<?php 
$user_data = $this->session->all_userdata();
$user_id = $user_data['user_id'];
$session_email = $user_data['email'];
?>
<div class="row">
	<div class="col-lg-12">
		<?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
           <?php echo $this->session->flashdata('error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
        </div>
        <?php } ?>
		<input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
		<!-- Panel with Left Sidebar -->
		<div class="card utils__card-with-sidebar">
			<div class="utils__card-sidebar">
				<div class="btn-group-vertical btn-group-justified">
					<button id="btn_change_password" type="button" class="btn btn-active">Change Password</button>
				</div>
				<div class="btn-group-vertical btn-group-justified mt-3">
					<button id="btn_change_email" type="button" class="btn">Change Email Address</button>
				</div>
			</div>
			<div id="main_change_password_div">
				<div class="card-header">
					<span class="utils__title">
						<strong>Change Password</strong>
					</span>
				</div>
				<div class="card-body">
					<div id="current_password_section">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Enter Current Password: *</label>
								<input type="password" class="form-control" placeholder="Current Password" name="current_password" id="current_password">
								<label id="lbl_current_password" style="color: red;"></label>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<a href="#" class="btn btn-rounded btn-success mr-2 mb-2" id="btn_check_password" style="margin-top: -15px;">Proceed</a>
							</div>
						</div>
					</div>
					<div id="change_password_section">
						<?php $form_submit_url = base_url() . "my_account/change_password"; ?>
						<form id="group-formsubmit" action="<?php echo $form_submit_url; ?>" method="POST">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Enter New Password: *</label>
									<input type="password" class="form-control" placeholder="New Password" name="new_password" id="new_password">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Verify New Password: *</label>
									<input type="password" class="form-control" placeholder="Verify New Password" name="verify_new_password" id="verify_new_password">
									<label id="lbl_verify_new_password" style="color: red;"></label>
								</div>
							</div>
							<div class="col-lg-6" id="submit_btn_div">
								<div class="form-group">
									<input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="main_change_email_div">
				<div class="card-header">
					<span class="utils__title">
						<strong>Change Email Address</strong>
					</span>
				</div>
				<div class="card-body">
					<div id="current_email_section">
						<?php $form_submit_url_change_email = base_url() . "my_account/change_email"; ?>
						<form id="change-email-formsubmit" action="<?php echo $form_submit_url_change_email; ?>" method="POST">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Enter New Email Address: * <i>(Your Current Email Address Is : <?php echo (isset($session_email) && $session_email!='') ? $session_email : '' ?> )</i></label>
									<input type="text" class="form-control" placeholder="New Email Address" name="new_email_address" id="new_email_address">
									<label id="new_email_address" style="color: red;"></label>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Panel with Left Sidebar -->
	</div>
</div>