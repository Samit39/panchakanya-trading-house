<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends MY_Controller {

	public function __construct() {
        parent::__construct();
        //
		$this->permission_name = "My Account";
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
		//
        $this->load->model('my_account_model', 'my_account');
        
    }

	public function index()
	{
		
		$this->set_log();
		$this->load->library('form_validation');
		$data = array();
		$data['breadcrumbs_title'] = "My Account";
		$data['content'] = '_view';
		// $data['group_list'] = $this->groups->getAllGroups();
		// pr($data['group_list']);
		// die();
		$data['addJs'] = array('assets/js/my_account.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function change_password()
	{
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		$user_id = $user_data['user_id'];
		$user_name = $user_data['username'];
		if($post['new_password']!='')
		{
			$this->my_account->update_password($user_id,$post['new_password']);
			//
			$this->set_log($user_id,"Password Changed Of - ".$user_name,14);
			//
			$this->session->set_flashdata('success', 'Password Successfully Updated.');
			redirect('dashboard');
		}
		else
		{
			$this->session->set_flashdata('error', 'New Email Address Cannot Be Empty. Try Again or Contact Bank If This Issue Continues.');
			redirect('dashboard');
		}
			
	}

	public function checkValidPassword()
	{
		$current_password=$_POST['current_password'];
        // $branch_result=$this->my_account->getAllBranchesByProvince($current_password);
        if($this->my_account->checkIfThisPasswordIsValid($current_password))
        {
        	echo json_encode(array(
        		'status' => true,
        		'message' => 'Valid Password'
        	));
        }
        else
        {
        	echo json_encode(array(
        		'status' => false,
        		'message' => 'Invalid Password'
        	));
        }
	}

	public function change_email()
	{
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		$user_id = $user_data['user_id'];
		$user_name = $user_data['username'];
		$this->my_account->update_email($user_id,$post['new_email_address']);
		//
		$this->set_log($user_id,"Email Address Changed Of - ".$user_name,14);
		//
		$this->session->set_flashdata('success', 'Email Address Successfully Updated.');
		redirect('dashboard');
	}
}
