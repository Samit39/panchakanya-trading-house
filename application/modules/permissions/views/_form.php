<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
              <?php echo $this->session->flashdata('success'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php
            // pr($permissions_data);
            if(isset($permissions_id) && $permissions_id != '')
            {
                $form_submit_url = base_url().'permissions/create/'.$permissions_id;
            }
            else
            {
                $form_submit_url = base_url().'permissions/create';
            } 
            ?>
            <form id="group-formsubmit" autocomplete="off" action="<?php echo $form_submit_url; ?>" method="POST">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Name :*</label>
                            <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo(isset($permissions_data) && $permissions_data->name!='') ? $permissions_data->name : '' ?>" required>
                            <?php echo form_error("name"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description :*</label>
                            <input type="text" class="form-control" placeholder="Description" name="description" value="<?php echo(isset($permissions_data) && $permissions_data->description!='') ? $permissions_data->description : '' ?>">
                             <?php echo form_error("description"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'permissions'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>