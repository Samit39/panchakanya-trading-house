<?php

class Permissions_model extends MY_Model {

    // public $table = 'groups';
    // public $id = '',$name = '',$description='';

    public function __construct() {
        parent::__construct();
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
             array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'trim|required',
            ),

 
        );

        return $array;
    }
    */

	public function getAllpermissions()
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_permissions t1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function insert_new_permissions($data)
	{
		$this->db->insert('tbl_permissions', $data);
	}

	public function get_detail_by_id($permissions_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_permissions t1');
		$this->db->where('t1.id',$permissions_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function update_permissions($permissions_id,$data)
	{
		$this->db->where('id', $permissions_id);
		$this->db->update('tbl_permissions', $data);
	}

	public function delete_permissions($permissions_id)
	{
		$this->db->where('id', $permissions_id);
		$this->db->delete('tbl_permissions');
	}
}