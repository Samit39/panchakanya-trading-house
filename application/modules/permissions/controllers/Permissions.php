<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Permissions";
        // is this module accessible for this group check_permission();
        //
        // if(!$this->check_permission($this->permission_name))
        // {
        //     $this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
        //     redirect('dashboard');
        // }
        $this->load->model('permissions_model', 'permissions');
        
    }

	public function index()
	{
		$this->load->library('form_validation');
		$data = array();
		$data['breadcrumbs_title'] = "Permissions";
		$data['content'] = '_list';
		$data['permissions_list'] = $this->permissions->getAllpermissions();
		// pr($data['permissions_list']);
		// die();
		// $data['addJs'] = array('assets/js/permissions.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$permissions_id = segment(3);
		$post = $_POST;
		if($permissions_id!='')
		{
			
			$data['permissions_data'] = $this->permissions->get_detail_by_id($permissions_id);
			$data['permissions_id'] = $permissions_id;
			
		}
		if(!empty($post))
		{
			// $this->form_validation->set_rules($this->permissions->rules());

		// if ($this->form_validation->run() == true)
		// {

			if($permissions_id!='')
			{
			
				//EDIT ko lagi
								
				$this->permissions->update_permissions($permissions_id,$post);
				$this->session->set_flashdata('success', 'Permissions Successfully Edited.');
				redirect('permissions');
			}
			else
			{

				//Add ko lagi
				$this->permissions->insert_new_permissions($post);
				$this->session->set_flashdata('success', 'Permissions Successfully Added.');
				redirect('permissions');
			}
		// } // for form validation
	}	
		$data['breadcrumbs_title'] = "Add permissions";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/permissions.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
		

	}

	public function delete()
	{
		$permissions_id = segment(3);
		if($permissions_id !='')
		{
			$this->permissions->delete_permissions($permissions_id);
			$this->session->set_flashdata('success', 'Permissions Successfully Deleted.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting Permissions.');
		}
		redirect('permissions');
	}
}
