<?php

class Statements_model extends MY_Model {

    public function __construct() {
        parent::__construct();
      
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
            
 
        );

        return $array;
    }
    
    public function getAllInactiveInformation()
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_statements t1');
		$this->db->order_by('t1.updated_date','DESC');
		$this->db->where('t1.status',0);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }
	*/
	public function getAllInformation($account_type,$account_id)
	{
		if($account_type!='' && ($account_type == "Item Providers" || $account_type == "Customers"))
		{
			$dayBookResult = $this->getAllDayBookInformationByAccountId($account_type,$account_id);
			$paymentResult = $this->getAllPaymentInformationByAccountTypeAndId($account_type,$account_id);
			//
			//
			$finalArray = array();
			$i=0;
			if(!empty($dayBookResult))
			{
				//This is for item providers
				if($account_type == "Item Providers")
				{
					foreach ($dayBookResult as $value) 
					{
						$finalArray[$i]['date'] = $value->date;
						$finalArray[$i]['ch_number'] = $value->ch_number;
						$finalArray[$i]['description'] = $value->item_name . ' Sent - <br>' . ' - <br>Remarks: ' .  $value->description;
						$finalArray[$i]['debit'] = $value->price;
						$finalArray[$i]['credit'] = "";
						$i++;
					}
				}
				//This is for customers
				if($account_type == "Customers")
				{
					foreach ($dayBookResult as $value) 
					{
						$finalArray[$i]['date'] = $value->date;
						$finalArray[$i]['ch_number'] = $value->ch_number;
						$finalArray[$i]['description'] = $value->item_name . ' Sent - <br>' . ' - <br>Remarks: ' .  $value->description;
						$finalArray[$i]['debit'] = "";
						$finalArray[$i]['credit'] = $value->price;
						$i++;
					}
				}	
			}
			if(!empty($paymentResult))
			{
				foreach ($paymentResult as $value) 
				{
					$finalArray[$i]['date'] = $value->date;
					$finalArray[$i]['ch_number'] = "---";
					$finalArray[$i]['description'] ='Payment Made || Remarks: ' .  $value->description;
					if($value->trans_type == "Dr")
					{
						$finalArray[$i]['debit'] = $value->payment_amount;
						$finalArray[$i]['credit'] = "";
					}
					else if($value->trans_type == "Cr")
					{
						$finalArray[$i]['debit'] = "";
						$finalArray[$i]['credit'] = $value->payment_amount;
					}
					$i++;
				}
			}
			//
			// pr($dayBookResult);
			// pr($paymentResult);
			// echo "final_array";
			// pr($finalArray);
			// die();
			//
			return $finalArray;
		}
		else
		{
			return false;
		}
		/*
		if($account_type == "Item Providers")
		{
			$dayBookResult = $this->getAllDayBookInformationByAccountId($account_type,$account_id);
			echo "here";
			die();
		}
		else if($account_type == "Customers")
		{
			$dayBookResult = $this->getAllDayBookInformationByAccountId($account_type,$account_id);
			echo "here";
			return $dayBookResult;
		}
		else
		{
			return false;
		}
		*/
	}

	public function getAllDayBookInformationByAccountId($account_type,$account_id)
	{
		$final_result = array();
		$this->db->select('t1.*,t2.title as item_name');
		$this->db->from('tbl_daybooks t1');
		$this->db->join('tbl_items t2','t2.id=t1.item_id');
		$this->db->order_by('t1.date','ASC');
		$this->db->where('t1.status',1);
		if($account_type == "Item Providers")
		{
			$this->db->where('t1.item_provider_id',$account_id);
		}
		else if($account_type == "Customers")
		{
			$this->db->where('t1.customer_id',$account_id);
		}
		else
		{
			return false;
		}
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getAllPaymentInformationByAccountTypeAndId($account_type,$account_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_payments t1');
		$this->db->order_by('t1.date','ASC');
		$this->db->where('t1.status',1);
		if($account_type == "Item Providers")
		{
			$this->db->where('t1.account_type',"Item Providers");
		}
		else if($account_type == "Customers")
		{
			$this->db->where('t1.account_type',"Customers");
		}
		else
		{
			return false;
		}
		$this->db->where('t1.account_id',$account_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function getStatementHoldersName($account_type,$account_id)
	{
		if($account_type == "Item Providers")
		{
			return $this->getItemProviderNameFromId($account_id);
		}
		else if($account_type == "Customers")
		{
			return $this->getCustomerNameFromId($account_id);
		}
		else
		{
			return false;
		}
	}

	public function getItemProviderNameFromId($account_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_providers t1');
		$this->db->where('t1.id',$account_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0]->title;
		}
		else
		{
			return false;
		}
	}

	public function getCustomerNameFromId($account_id)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_customers t1');
		$this->db->where('t1.id',$account_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0]->title;
		}
		else
		{
			return false;
		}
	}
	/*
	public function insert_new_information($data)
	{
		$this->db->insert('tbl_statements', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function get_detail_by_id($id)
	{	
		$this->db->select('t1.*');
		$this->db->from('tbl_statements t1');
		$this->db->where('t1.id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function update_information($item_id,$data)
	{
		$this->db->where('id', $item_id);
		$this->db->update('tbl_statements', $data);
	}

	public function delete_items($item_id)
	{
		$data['status'] = 0;
		$this->db->where('id', $item_id);
		$this->db->update('tbl_statements', $data);
	}

	public function recycle_items($item_id)
	{
		$data['status'] = 1;
		$this->db->where('id', $item_id);
		$this->db->update('tbl_statements', $data);
	}
	*/
}