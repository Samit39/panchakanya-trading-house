<div class="row">
    <div class="col-lg-12">
        <?php if($this->session->flashdata('success')!=''){ ?>
        <div class="alert alert-success">
            <strong>Success!!! </strong><?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error')!=''){ ?>
        <div class="alert alert-danger">
            <strong>Error!!! </strong><?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
        <!-- <a href="<?php echo base_url().'statements/create' ?>" class="btn btn-rounded btn-primary mr-2 mb-3 pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Information</a> -->
        <hr style="margin-top: 58px !important;">
        <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
        <input type="hidden" name="statement_holder_name" id="statement_holder_name" value="<?php echo (isset($statement_holder_name) && $statement_holder_name!='') ? $statement_holder_name : "" ?>">
        <?php $form_submit_url = base_url().'statements/search' ?>
        <div>
            <form id="statements_formsubmit" action="<?php echo $form_submit_url; ?>" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Account Type: *</label>
                            <select class="form-control" id ="account_type_select" name="account_type_select" required>
                                <option value="">--Select Account Type--</option>
                                <option value="Item Providers" <?php echo (isset($statements_data) && $statements_data['account_type']=='Item Providers') ? "selected" : '' ?> >Item Providers</option>
                                <option value="Customers" <?php echo (isset($statements_data) && $statements_data['account_type']== 'Customers') ? "selected" : '' ?> >Customers</option>
                            </select>
                            <?php echo form_error("account_type_select"); ?>
                        </div>
                        <div class="form-group">
                            <label>Select Account Holder's Name: *</label>
                            <input type="hidden" name="account_id" id="account_id" value="<?php echo(isset($statements_data) && $statements_data['account_id']!='') ? $statements_data['account_id'] : '' ?>">
                            <select class="form-control" id ="account_holders_name_select" name="account_holders_name_select" required>
                                <option value="">--Select Account Holders Name--</option>
                            </select>
                            <?php echo form_error("account_holders_name_select"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input class="btn btn-rounded btn-primary mr-2 mb-3 pull-right" type="submit" name="submit" value="Search">
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <?php if(isset($information_list) && !empty($information_list)) { 
            // pr($information_list);
            ?>
        <div class="mb-5 table-responsive" style="margin-top: 55px;">
            <table class="table table-hover display" id="statements_table">
                <thead>
                    <tr>
                        <th>Transaction Date</th>
                        <th>Chalani No:</th>
                        <th>Description</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <?php
                if(isset($information_list) && !empty($information_list))
                {
                ?>
                <tbody>
                    <?php
                    $totalDebit = 0;
                    $totalCredit = 0;
                    foreach ($information_list as $key => $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['date']; ?></td>
                        <td><?php echo $value['ch_number']; ?></td>
                        <td><?php echo $value['description']; ?></td>
                        <td>
                            <?php 
                                echo ($value['debit']!='' || $value['debit']!=0) ? "Rs. " . $value['debit'] : '---'; 
                                $totalDebit += $value['debit'];
                            ?>
                        </td>
                        <td>
                            <?php 
                                echo ($value['credit']!='' || $value['credit']!=0) ? "Rs. " . $value['credit'] : '---'; 
                                $totalCredit += $value['credit'];
                            ?>
                        </td>
                        <td>---</td>
                    </tr>
                    <?php
                    } //for each loop
                    ?>  
                </tbody>
                <tfoot>
                    <tr>
                        <td>---</td>
                        <td>---</td>
                        <td>---</td>
                        <td>Total Debit Entries: Rs.<?php echo $totalDebit; ?></td>
                        <td>Total Credit Entries: Rs.<?php echo $totalCredit; ?></td>
                        <td>Total Closing Balance: Rs.<?php echo $totalDebit - $totalCredit; ?></td>
                    </tr>
                </tfoot>
                <?php
                } //if condition
                ?>
            </table>
        </div>
        <?php } ?>
    </div>
</div>
<script>
(function($) {
"use strict";
$(function () {
    var top_message = "The information in this table is intended to PANCHAKANYA TRADING HOUSE PVT. LTD. and copyright to Braincoders Technologies.";
    var statement_holder_name = $('#statement_holder_name').val();
    var file_title = statement_holder_name + " || All Transactions Statements";
    $('#statements_table').DataTable({
        
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            // 'copy',
            {
                extend: 'excel',
                messageTop: top_message,
                title: file_title,
                footer: true
            },
            // {
            //     extend: 'csv',
            //     messageTop: top_message,
            //     title: file_title
            // },
            {
                extend: 'pdf',
                messageTop: top_message,
                title: file_title,
                footer: true
            },
            {
                extend: 'print',
                messageTop: top_message,
                title: file_title,
                footer: true
            }
        ]
        
    });
/*
$('#example2').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
$('#example3').DataTable({
autoWidth: true,
scrollX: true,
fixedColumns: true
});
*/
});
})(jQuery)
</script>