<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statements extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Statements";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('statements_model', 'statements');
    }

	public function index()
	{   
		$data = array(); 
		$this->list_page($data);
	}

	public function search()
	{
		$data = array();
		$post = $_POST;
		$data['statements_data']['account_type'] = $post['account_type_select'];
		$data['statements_data']['account_id'] = $post['account_holders_name_select'];
		if($post['account_type_select']!='' && $post['account_holders_name_select']!='')
		{
			// echo $post['account_type_select'] . "<br>" . $post['account_holders_name_select'];
			$data['statement_holder_name'] = $this->statements->getStatementHoldersName($post['account_type_select'],$post['account_holders_name_select']);
			$data['information_list'] = $this->statements->getAllInformation($post['account_type_select'],$post['account_holders_name_select']);
		}
		// pr($data);
		// die();
		$this->list_page($data);
	}

	public function list_page($data)
	{
		$this->load->library('form_validation');
		// $data = array(); 
		$data['breadcrumbs_title'] = "Statements Manager";
		$data['content'] = '_list';
		$data['addCss'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.css'
		);
		$data['addJs'] = array(
			'assets/nepali-datepicker/nepali.datepicker.v2.2.min.js',
			'assets/js/statements.js'
		);
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$item_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->statements->getAllUsersList();
		//
		if($item_id!='')
		{
			$data['statements_data'] = $this->statements->get_detail_by_id($item_id);
			$data['item_id'] = $item_id;
			// pr($data['statements_data']);
			// echo $data['statements_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->statements->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($item_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_statements
				$tbl_statements_data['title'] = $post['title'];
				$tbl_statements_data['description'] = $post['description'];
				$tbl_statements_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_statements_data['status'] = 1;
				$tbl_statements_data['updated_by'] = $user_data['user_id'];
				//
				$this->statements->update_information($item_id,$tbl_statements_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('statements');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_statements
				$tbl_statements_data['title'] = $post['title'];
				$tbl_statements_data['description'] = $post['description'];
				$tbl_statements_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_statements_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_statements_data['status'] = 1;
				$tbl_statements_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_statements_id = $this->statements->insert_new_information($tbl_statements_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('statements');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/statements.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$statements_id = segment(3);
		if($statements_id !='')
		{
			$this->statements->delete_statements($statements_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('statements');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "statements Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->statements->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$statements_id = segment(3);
		if($statements_id !='')
		{
			$this->statements->recycle_statements($statements_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In statements.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('statements/recycle_bin_list');
	}
}
