<div class="login__block">
  <div class="row">
    <div class="col-xl-12">
      <div class="login__block__inner">
        <div class="login__block__form">
          <h4 class="text-uppercase">
          <strong>Reset Password </strong>
          </h4>
          <br>
          <h6>Please Change Your Passowrd</h6>
          <br />
          <?php if($this->session->flashdata('login_error')!=''){ ?>
          <div class="alert alert-danger">
           <?php echo $this->session->flashdata('login_error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
          </div>
          <?php } ?>
          <form id="form-validation" action="<?php echo base_url().'login/change_password'; ?>" name="form-validation" method="POST">
            <div class="form-group">
              <label class="form-label">Enter New Password</label>
              <input id="validation-email" class="form-control" placeholder=" Enter New Password" name="new_password" type="password">
            </div>
            <div class="form-group">
              <label class="form-label">Confirm New Password</label>
              <input id="validation-email" class="form-control" placeholder=" Confirm New Password" name="confirm_new_password" type="password">
            </div>
            <div class="form-group">
              <a href="<?php echo base_url();?>" class="pull-right utils__link--blue utils__link" style="margin-top: 20px;">Reset Password Later <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a>
              <!-- <div class="checkbox">
                <label>
                  <input type="checkbox" name="example6" checked>
                  Remember me
                </label>
              </div> -->
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary mr-3">Change Password</button>
              <!-- <span class="register-link">
                <a href="" class="utils__link--blue utils__link--underlined">Register</a> if you don't have account
              </span> -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>