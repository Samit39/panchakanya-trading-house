<div class="login__block">
  <div class="row">
    <div class="col-xl-12">
      <div class="login__block__inner">
        <div class="login__block__form">
          <h4 class="text-uppercase">
          <strong>LOGIN</strong>
          </h4>
          <br />
          <?php if($this->session->flashdata('success')!=''){ ?>
          <div class="alert alert-success">
           <?php echo $this->session->flashdata('success'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
          </div>
          <?php } ?>
          <?php if($this->session->flashdata('login_error')!=''){ ?>
          <div class="alert alert-danger">
            <strong>Error!!! </strong><?php echo $this->session->flashdata('login_error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
          </div>
          <?php } ?>
          <form id="form-validation" action="<?php echo base_url().'login'; ?>" name="form-validation" method="POST">
            <div class="form-group">
              <label class="form-label">Username</label>
              <input id="validation-email"
              class="form-control"
              placeholder="Email or Username"
              name="username"
              type="text"
              data-validation="[EMAIL]">
            </div>
            <div class="form-group">
              <label class="form-label">Password</label>
              <input id="validation-password"
              class="form-control password"
              name="password"
              type="password" data-validation="[L>=6]"
              data-validation-message="$ must be at least 6 characters"
              placeholder="Password">
            </div>
            <div class="form-group">
              <a href="<?php echo base_url().'login/forgot_password' ?>" class="pull-right utils__link--blue utils__link" style="margin-top: 20px;">Forgot Password <i class="fa fa-question-circle" aria-hidden="true"></i> </a>
              <!-- <div class="checkbox">
                <label>
                  <input type="checkbox" name="example6" checked>
                  Remember me
                </label>
              </div> -->
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary mr-3">Sign In</button>
              <!-- <span class="register-link">
                <a href="" class="utils__link--blue utils__link--underlined">Register</a> if you don't have account
              </span> -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>