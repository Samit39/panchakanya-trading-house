<div class="login__block">
  <div class="row">
    <div class="col-xl-12">
      <div class="login__block__inner">
        <div class="login__block__form">
          <h4 class="text-uppercase">
          <strong>Reset Password </strong>
          </h4>
          <br>
          <h6>Please Provide System Registered Email Address To Reset Password.</h6>
          <br />
          <?php if($this->session->flashdata('login_error')!=''){ ?>
          <div class="alert alert-danger">
           <?php echo $this->session->flashdata('login_error'); ?>
             <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
          </div>
          <?php } ?>
          <form id="form-validation" action="<?php echo base_url().'login/forgot_password'; ?>" name="form-validation" method="POST">
            <div class="form-group">
              <label class="form-label">Enter Registered Email</label>
              <input id="validation-email"
              class="form-control"
              placeholder=" Enter Registered Email"
              name="email"
              type="email"
              data-validation="[EMAIL]">
            </div>
            <div class="form-group">
              <a href="<?php echo base_url();?>" class="pull-right utils__link--blue utils__link" style="margin-top: 20px;">Go Back And Continue Login <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a>
              <!-- <div class="checkbox">
                <label>
                  <input type="checkbox" name="example6" checked>
                  Remember me
                </label>
              </div> -->
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary mr-3">Reset Password</button>
              <!-- <span class="register-link">
                <a href="" class="utils__link--blue utils__link--underlined">Register</a> if you don't have account
              </span> -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>