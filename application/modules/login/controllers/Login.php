<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'login');
    }

	public function index()
	{
		// $user_data = $this->session->userdata('session_data');
		if($this->session->has_userdata('is_logged_in'))
		{
			//
			$user_data = $this->session->all_userdata();
			$user_id = $user_data['user_id'];
			$username = $user_data['username'];
			$this->set_log($user_id,"Logged In By - ".$username,8);
			//
			redirect('dashboard');
		}
		else
		{
			$post = $_POST;
			if(!empty($post))
			{
				// pr($post);
				// echo $post['username'];
				if($this->login->check_login($post['username'],$post['password']))
				{
					// check if the login is first time
					if($this->login->check_first_login($post['username'])=="Yes")
					{
						// $this->change_password();
						redirect('login/change_password');
					}
					// else
					// {
					// 	echo "Not First Login";
					// 	die();
					// }
					//
					$user_data = $this->session->all_userdata();
					$user_id = $user_data['user_id'];
					$username = $user_data['username'];
					$this->set_log($user_id,"Logged In By - ".$username,8);
					//
					redirect('dashboard');
				}
				else
				{
					//
					$this->set_log('',"Tried To Login With Invalid Credential With Username - ".$post['username'],15);
					//
					$this->session->set_flashdata('login_error', 'Invalid Username / Password');
					redirect('login');
				}
			}
			$this->load->view('common/header');
			$this->load->view('login_form');
		}
	}
	//FOR FORGOT PASSWORD STARTS HERE
	public function forgot_password()
	{
		if($this->session->has_userdata('is_logged_in'))
		{
			redirect('dashboard');
		}
		else
		{
			$post = $_POST;
			if(!empty($post))
			{
				// pr($post);
				// die();
				// echo $post['username'];
				if($this->login->check_valid_email($post['email']))
				{
					$this->login->reset_password_and_email($post['email']);
					//
					$this->set_log('',"Password Reset From Login Form With Email - ".$post['email'],16);
					//
					$this->session->set_flashdata('success', 'New Password Is Sent In Your Registered Email Address.');
					redirect('login');
				}
				else
				{
					$this->session->set_flashdata('login_error', 'Invalid Email Address. Please Check And Try Again.');
				}
			}
			$this->load->view('common/header');
			$this->load->view('forgot_password_form');
		}
	}
	//FOR FORGOT PASSWORD ENDS HERE
	//FOR CHANGE PASSWORD Starts HERE
	public function change_password()
	{
		//
		$user_data = $this->session->all_userdata();
		if(!empty($user_data))
		{
			$user_id = $user_data['user_id'];
			$username = $user_data['username'];
			if($this->login->check_first_login($username)=="No")
			{
				redirect('dashboard');
			}
			else
			{
				$post = $_POST;
				if(!empty($post))
				{
					if($post['new_password']=='' || $post['confirm_new_password'] =='')
					{
						$this->session->set_flashdata('login_error', 'New Password And Confirm New Password Required. Please Check And Try Again.');
					}
					else if($post['new_password'] == $post['confirm_new_password'])
					{
						//perform change password operation
						$this->login->update_password($user_id,$post['new_password']);
						//
						$this->set_log($user_id,"Password Changed Of - ".$username,14);
						//
						$this->session->set_flashdata('success', 'Password Successfully Updated.');
						redirect('dashboard');
					}
					else
					{
						$this->session->set_flashdata('login_error', 'New Password And Confirm New Password Do not Match. Please Check And Try Again.');
					}
					// echo $post['username'];
					/*
					if($this->login->check_valid_email($post['email']))
					{
						$this->login->reset_password_and_email($post['email']);
						//
						$this->set_log('',"Password Reset From Login Form With Email - ".$post['email'],16);
						//
						$this->session->set_flashdata('success', 'New Password Is Sent In Your Registered Email Address.');
						redirect('login');
					}
					else
					{
						$this->session->set_flashdata('login_error', 'Invalid Email Address. Please Check And Try Again.');
					}
					*/
				}
				$this->load->view('common/header');
				$this->load->view('change_password_form');
			}
		} // userdata !empty ko if
		else
		{
			redirect('login');
		}

			
	}
	//FOR CHANGE PASSWORD ENDS HERE
	//FOR LOGOUT
	public function logout()
	{
		//
		$user_data = $this->session->all_userdata();
		$user_id = $user_data['user_id'];
		$username = $user_data['username'];
		$this->set_log($user_id,"Logged Out By - ".$username,9);
		//
		$this->session->sess_destroy();
        redirect('login');
	}
	//FOr Logout ends
	public function set_log($moduleId='',$moduleTitle='',$action='')
    {
        $user_data = $this->session->all_userdata();
        $moduleName = $this->router->fetch_class();
        //
        $data['moduleName'] = $moduleName;
        $data['moduleId'] = ($moduleId=='') ? 1 : $moduleId; //Set 1 for clicked
        $data['moduleTitle'] = ($moduleTitle == '') ? "Listing Page Viewed On ".$moduleName : $moduleTitle;
        $data['action'] = ($action=='') ? 1 : $action;
        $data['dateTime'] = date("Y-m-d H:i:s");
        $data['ip'] = get_ip_address();
        $data['user_id'] = $user_data['user_id'];
        //
        $this->db->insert('tbl_logreport',$data);
    }
}
