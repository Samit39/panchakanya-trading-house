<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Customers";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('customers_model', 'customers');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Customers Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->customers->getAllInformation();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$customer_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->customers->getAllUsersList();
		//
		if($customer_id!='')
		{
			$data['customers_data'] = $this->customers->get_detail_by_id($customer_id);
			$data['customer_id'] = $customer_id;
			// pr($data['customers_data']);
			// echo $data['customers_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->customers->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($customer_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_customers
				$tbl_customers_data['title'] = $post['title'];
				$tbl_customers_data['description'] = $post['description'];
				$tbl_customers_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_customers_data['status'] = 1;
				$tbl_customers_data['updated_by'] = $user_data['user_id'];
				//
				$this->customers->update_information($customer_id,$tbl_customers_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('customers');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_customers
				$tbl_customers_data['title'] = $post['title'];
				$tbl_customers_data['description'] = $post['description'];
				$tbl_customers_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_customers_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_customers_data['status'] = 1;
				$tbl_customers_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_customers_id = $this->customers->insert_new_information($tbl_customers_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('customers');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/customers.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$customers_id = segment(3);
		if($customers_id !='')
		{
			$this->customers->delete_customers($customers_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('customers');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "customers Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->customers->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$customers_id = segment(3);
		if($customers_id !='')
		{
			$this->customers->recycle_customers($customers_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In customers.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('customers/recycle_bin_list');
	}
}
