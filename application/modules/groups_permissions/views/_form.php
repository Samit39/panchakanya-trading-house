<div class="row">
    <div class="col-lg-12">
        <div class="mb-5">
            <?php if($this->session->flashdata('success')!=''){ ?>
            <div class="alert alert-success">
               <?php echo $this->session->flashdata('success'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('error')!=''){ ?>
            <div class="alert alert-danger">
               <?php echo $this->session->flashdata('error'); ?>
                 <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">x</span></button>
            </div>
            <?php } ?>
            <?php
            // pr($groups_permissions_data);
            if(isset($groups_permissions_id) && $groups_permissions_id != '')
            {
                $form_submit_url = base_url().'groups_permissions/create/'.$groups_permissions_id;
            }
            else
            {
                $form_submit_url = base_url().'groups_permissions/create';
            } 
            ?>
            <form id="group-formsubmit" autocomplete="off" action="<?php echo $form_submit_url; ?>" method="POST">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Group Name :*</label>
                            <input type="text" class="form-control" disabled placeholder="Group Name" name="name" value="<?php echo(isset($groups_permissions_data) && $groups_permissions_data[0]->name!='') ? $groups_permissions_data[0]->name : '' ?>" required>
                            <?php echo form_error("name"); ?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Select Permissions:</label>
                            <select multiple="true" class="form-control" name="permissions[]" id="permission">
                                <?php
                                $permissions_array = array();
                                if(isset($groups_permissions_data))
                                {
                                    $permissions_array = explode(",", $groups_permissions_data[0]->permissions);
                                }
                                if(isset($permissions_list))
                                {
                                    foreach ($permissions_list as $value) {
                                ?>
                                <option value="<?php echo $value->id; ?>" <?php echo (in_array($value->id, $permissions_array)) ? 'selected' : '' ?> ><?php echo $value->name; ?></option>
                                <?php
                                    }
                                } 
                                ?>
                              
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-success mr-2 mb-2">
                            <a href="<?php echo base_url().'groups_permissions'; ?>" class="btn btn-rounded btn-danger mr-2 mb-2">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
         $("#permission").select2();
    });
</script>