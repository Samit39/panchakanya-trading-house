<?php

class Groups_permissions_model extends MY_Model {

    // public $table = 'groups';
    // public $id = '',$name = '',$description='';

    public function __construct() {
        parent::__construct();
    }

    /*
    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|required',
            ),
             array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'trim|required',
            ),

 
        );

        return $array;
    }
    */

	public function getAllgroups_permissions()
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			// return $query->result();
			foreach ($query->result() as $value) 
			{
				$value->permissions = $this->getPermissionsForThisGroup($value->id);
				array_push($final_result, $value);
			}
			return $final_result;
		}
		else
		{
			return false;
		}
	}

	public function getPermissionsForThisGroup($groupid)
	{
		$permission_string = "";
		$this->db->select('t1.*');
		$this->db->from('tbl_group_permission t1');
		$this->db->where('t1.group_id',$groupid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $value) 
			{
				$permission_string = $permission_string . $this->getPermissionNameFromPermissionId($value->permission_id) . ",";
			}
			return $permission_string;
		}
		else
		{
			return false;
		}
	}

	public function getPermissionNameFromPermissionId($permissionid)
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_permissions t1');
		$this->db->where('t1.id',$permissionid);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0]->name;
		}
		else
		{
			return 0;
		}
	}

	public function getAllPermissionsLists()
	{
		$this->db->select('t1.*');
		$this->db->from('tbl_permissions t1');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		else
		{
			return 0;
		}
	}

	public function insert_groups_permissions($data)
	{
		$this->db->insert('tbl_group_permission', $data);
	}

	public function get_detail_by_id($group_id)
	{
		$final_result = array();
		$this->db->select('t1.*');
		$this->db->from('tbl_groups t1');
		$this->db->where('t1.id',$group_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			// return $query->result();
			foreach ($query->result() as $value) 
			{
				$value->permissions = $this->getPermissionsIdsForThisGroup($value->id);
				array_push($final_result, $value);
			}
			return $final_result;
		}
		else
		{
			return false;
		}
	}

	public function getPermissionsIdsForThisGroup($group_id)
	{
		$permission_ids_string = "";
		$this->db->select('t1.*');
		$this->db->from('tbl_group_permission t1');
		$this->db->where('t1.group_id',$group_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $value) 
			{
				$permission_ids_string = $permission_ids_string . $value->permission_id . ",";
			}
			return $permission_ids_string;
		}
		else
		{
			return false;
		}
	}

	/*
	public function update_permissions($permissions_id,$data)
	{
		$this->db->where('id', $permissions_id);
		$this->db->update('tbl_permissions', $data);
	}
	*/

	public function delete_group_permissions($groupid)
	{
		$this->db->where('group_id', $groupid);
		$this->db->delete('tbl_group_permission');
	}
}