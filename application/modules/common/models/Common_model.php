<?php

class Common_model extends MY_Model {

	public function __construct()
    {
        parent::__construct();
    }

    public function get_current_account_balance()
    {
        //
        $user_data = $this->session->all_userdata();
        $username = $user_data['username'];
        $depo_code = $username;
        //
        $curl = curl_init();
        // FOR LOCAL MOCK API POST REQUEST STARTS HERE
        /*
        $url =  base_url().'public/testnoc/getCurrentAccountBalance';
        $curlArray = array(
            'DepoCode' => $depo_code
        );
        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_SSL_VERIFYPEER => 0, // Skip SSL Verification
        CURLOPT_HTTPHEADER => array("Accept: application/json"),
        CURLOPT_USERAGENT => 'Account Opening Module',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $curlArray
        ));
        */
        // FOR LOCAL MOCK API POST REQUEST ENDS HERE
        // FOR LIVE API GET REQUEST STARTS HERE
        
        // $depo_code = "DEmo";
        // echo $depo_code;
        // $url = 'http://10.10.0.203/FIService/api/noc/GetDepoTransactions?DepoCode='.$depo_code.'&FromDate='.$from_date.'&ToDate='.$to_date.'&DrCrFlag='.$dr_cr_flag;
        $url = 'http://10.10.0.203:9001/service/api/noc/GetDepoAccountBalance?DepoCode='.$depo_code;
        // echo $url;
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("Accept: application/json"),
            CURLOPT_POST => 0
        ));
        
        // FOR LIVE API GET REQUEST ENDS HERE
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // print_r($resp);die();
        if (curl_errno($curl)) 
        {
            $this->session->set_flashdata('error', 'Could not get Account Balance At this Time. Please try again later or if the problem persists, please contact the bank for any assistance.');
            redirect('dashboard');
        } 
        else 
        {
            
            curl_close($curl);
            $returndata = json_decode($resp);
            // pr(json_last_error_msg ());
            // echo "<pre>";
            // pr($resp);
            // pr($returndata);
            // echo $returndata->status;
            // die();
            // echo "<pre>";
            //FOR LIVE STARTS HERE
            
            return $returndata;
            //OLD STARTS
            // if(!empty($returndata))
            // {
            //  return $returndata;
            // }
            // else
            // {
            //  $this->session->set_flashdata('error', "Invalid Response Obtained From Bank, Please Contack Bank Or Try Again Later...");
            //  redirect('dashboard');
            // }
            //OLD ENDS
            
            //FOR LIVE ENDS HERE
            //FOR LOCAL STARTS HERE
            /*
            if ($returndata->status == true) 
            {
                //FOR LOCAL MOCK
                return $returndata->account_balance;
            } 
            else 
            {
                $this->session->set_flashdata('error', $returndata->message);
                redirect('dashboard');
            }
            */
            //FOR LOCAL ENDS HERE
        }
    }
}