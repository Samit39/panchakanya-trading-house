<?php

class Sitesettings_model extends MY_Model {

	public function __construct()
    {
        parent::__construct();
    }

    public function getSiteSettings()
    {
    	$this->db->select('t1.*');
		$this->db->from('tbl_site_setting t1');
		$this->db->where('id',1);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result()[0];
		}
		else
		{
			return false;
		}
    }
}