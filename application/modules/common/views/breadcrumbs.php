<nav class="utils__top-sidebar utils__top-sidebar--bg">
    <!--<div class="pull-right">
        <a href="javascript: void(0);" class="btn btn-sm btn-outline-default ladda-button">
            Update
            <span class="hidden-sm-down"> Dashboard</span>
        </a>
    </div> -->
    <span class="font-size-18 d-block mb-2">
        <span class="text-muted"><?php echo (isset($breadcrumbs_title) && $breadcrumbs_title!='') ? $breadcrumbs_title : "NIC ASIA NOC PAYMENT TRACKING APPLICATION" ?></span>
        <span class="pull-right text-muted"> <?php echo (isset($breadcrumbs_account_balance) && $breadcrumbs_account_balance!='') ? " Account Balance : Rs " . $breadcrumbs_account_balance : ""; ?></span>
    </span>
</nav>