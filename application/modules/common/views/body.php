<!-- Adding Custom Css -->
<?php if(isset($addCss) && !empty($addCss)) {
foreach($addCss as $css) { ?>
<link href="<?php echo base_url($css); ?>" rel="stylesheet" type="text/css" />
<?php }
} ?>
<body class="config--vertical config--borderLess menu-left--colorful menu-left--visible menu-left--shadow theme--red">
<!--FOR submit LOADER STARTS -->
<!-- <div id="loading">
  <img id="loading-image" src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="Loading..." />
</div> -->
<!--FOR submit LOADER ENDS -->
<!--FOR LOADER STARTS -->

<div class="page-loader home-loader">
    <div class="inner-loader">
        <img src="<?php echo base_url(); ?>assets/components/dummy-assets/common/img/loading.gif" class="mascot" alt="">
        <p class="text-center">Loading.....<span></span> <span></span> <span></span> </p>
    </div>
</div>
<!-- <div class="loader_overlay is-visible">
	<span class="ml8">
		<span class="letters bang">
			<img src="<?php echo base_url().'assets/loaderimg/' ?>sunriselogo.png">
		</span>
		
		<span class="letters">
			<img src="<?php echo base_url().'assets/loaderimg/' ?>logo2.svg">
		</span>
		
		<span class="circle circle-dark-dashed"></span> <span class="loader_slogan"><img src="<?php echo base_url().'assets/loaderimg/' ?>slogan.png"></span>
	</span>
</div> -->
<!-- FOR LOADER ENDS -->
	<?php $this->load->view('common/right_menu'); ?>
	<?php $this->load->view('common/left_menu'); ?>
	<?php $this->load->view('common/top_bar'); ?>
	<div class="utils__content">
		<!-- START: dashboard alpha -->
		<?php $this->load->view('common/breadcrumbs'); ?>
		<?php //$this->load->view('common/content'); ?>
		<?php
			if(isset($content) && $content!='')
			{
				$this->load->view($content);
			}
			else
			{
		?>
		<div class="row">
			<div class="col-lg-12">
				NO CONTENT LOADED
			</div>
		</div>
		<?php
			}
		?>
		<!-- END: dashboard alpha -->
		<!-- START: page scripts -->
	</div>
	<?php $this->load->view('common/footer'); ?>
</body>
</html>