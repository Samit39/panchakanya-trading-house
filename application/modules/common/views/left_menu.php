<?php
$called_class = $this->router->fetch_class();
if(segment(2)!='')
{
    $called_method = segment(2);
}
else
{
    $called_method = $this->router->fetch_method();
}
$active_class = "menu-left__item--active";
// $active_parent_class = "menu-left--colorful--primary";
$active_parent_class = " menu-left--colorful--success menu-left__submenu--toggled";
?>
<?php
$user_data = $this->session->all_userdata();
$accessible_module_array = explode(",", $user_data['accessible_module']);
?>
<nav class="menu-left">
    <div class="menu-left__lock menu-left__action--menu-toggle">
        <div class="toggle-btn-wrap">
            <div class="menu-left__pin-button">
                <div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-left__logo">
        <a href="<?php echo base_url().'dashboard';?>">
            <img src="<?php echo base_url().'assets/' ?>components/dummy-assets/common/img/icon/toplogo.png" width="152" height="35" alt="" />
        </a>
    </div>
    <div class="menu-left__inner">
        <ul class="menu-left__list menu-left__list--root">
            <li class="menu-left__item side-border <?php echo ($called_class=='dashboard') ? $active_class : "" ?>">
                <a href="<?php echo base_url().'dashboard';?>">
                    <span class="menu-left__icon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                    Dashboard
                </a>
            </li>
            <hr>
            <?php if(in_array(5, $accessible_module_array)){ ?>
                <li class="menu-left__item border-side  <?php echo ($called_class=='my_account') ? $active_class : "" ?>">
                    <a href="<?php echo base_url().'my_account' ?>">
                        <span class="menu-left__icon"><i class="fa fa-id-card" aria-hidden="true"></i></span>
                        My Account
                    </a>
                </li>
            <?php } ?>
            <?php if(in_array(19, $accessible_module_array)){ ?>
                <li class="menu-left__item border-side  <?php echo ($called_class=='daybooks') ? $active_class : "" ?>">
                    <a href="<?php echo base_url().'daybooks' ?>">
                        <span class="menu-left__icon"><i class="fa fa-book" aria-hidden="true"></i></span>
                        Daybooks
                    </a>
                </li>
            <?php } ?>
            <?php if(in_array(20, $accessible_module_array)){ ?>
                <li class="menu-left__item border-side  <?php echo ($called_class=='payments') ? $active_class : "" ?>">
                    <a href="<?php echo base_url().'payments' ?>">
                        <span class="menu-left__icon"><i class="fa fa-money" aria-hidden="true"></i></span>
                        Payments
                    </a>
                </li>
            <?php } ?>
            <?php if(in_array(21, $accessible_module_array)){ ?>
                <li class="menu-left__item border-side  <?php echo ($called_class=='statements') ? $active_class : "" ?>">
                    <a href="<?php echo base_url().'statements' ?>">
                        <span class="menu-left__icon"><i class="fa fa-files-o" aria-hidden="true"></i></span>
                        Statements
                    </a>
                </li>
            <?php } ?>
            <li class="menu-left__divider"></li>
            <?php if(in_array(6, $accessible_module_array)){ ?>
                <li class="menu-left__item <?php echo ($called_class=='log_report') ? $active_class : "" ?>">
                    <a href="<?php echo base_url().'log_report' ?>">
                        <span class="menu-left__icon"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                        Log Report
                    </a>
                </li>
            <?php } ?>
            <?php 
            $settings_array = array(15,16,17);
            if (!empty(array_intersect($accessible_module_array, $settings_array))){ ?>
                <li class="menu-left__item menu-left__submenu">
                    <a href="javascript: void(0);">
                        <span class="menu-left__icon"><i class="fa fa-wrench" aria-hidden="true"></i></span>
                        Settings
                    </a>
                    <ul class="menu-left__list">
                        <?php if(in_array(15, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='items') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'items' ?>">
                                    <span class="menu-left__icon">I</span>
                                    Items
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(in_array(16, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='providers') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'providers' ?>">
                                    <span class="menu-left__icon">P</span>
                                    Item Providers
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(in_array(17, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='customers') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'customers' ?>">
                                    <span class="menu-left__icon">C</span>
                                    Customers
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>

            <?php 
            $configuration_array = array(2,1,3,4);
            if (!empty(array_intersect($accessible_module_array, $configuration_array))){ ?>
                <li class="menu-left__item menu-left__submenu">
                    <a href="javascript: void(0);">
                        <span class="menu-left__icon"><i class="fa fa-cog" aria-hidden="true"></i></span>
                        Configuration
                    </a>
                    <ul class="menu-left__list">
                        <?php if(in_array(2, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='users') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'users' ?>">
                                    <span class="menu-left__icon"><i class="fa fa-user-circle" aria-hidden="true"></i></span>
                                    User
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(in_array(1, $accessible_module_array)){ ?>
                            <li class="menu-left__item">
                                <a href="<?php echo base_url().'groups'; ?>">
                                    <span class="menu-left__icon">G</span>
                                    Groups
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(in_array(3, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='permissions') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'permissions' ?>">
                                    <span class="menu-left__icon">P</span>
                                    Permissions
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(in_array(4, $accessible_module_array)){ ?>
                            <li class="menu-left__item border-side  <?php echo ($called_class=='groups_permissions') ? $active_class : "" ?>">
                                <a href="<?php echo base_url(). 'groups_permissions' ?>">
                                    <span class="menu-left__icon">GP</span>
                                    Groups Permissions
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <li class="menu-left__divider"></li>
            <li class="menu-left__item side-border">
                <a href="<?php echo base_url().'login/logout'; ?>">
                    <span class="menu-left__icon"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                    Logout
                </a>
            </li>
        </ul>
    </div>
</nav>
