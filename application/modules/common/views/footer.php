<script>
  /*
  (function($) {
    "use strict";
    $(function () {

      ///////////////////////////////////////////////////////////
      // tooltips
      $('[data-toggle=tooltip]').tooltip()

      ///////////////////////////////////////////////////////////
      // ladda buttons
      Ladda.bind('.ladda-button', {timeout: 2000})

      ///////////////////////////////////////////////////////////
      // Chartistcard

      new Chartist.Line(".chart-area-1", {
        series: [
          [2, 11, 8, 14, 18, 20, 26]
        ]
      }, {
        width: "120px",
        height: "107px",

        showPoint: false,
        showLine: true,
        showArea: true,
        fullWidth: true,
        showLabel: false,
        axisX: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        chartPadding: 0,
        low: 0,
        plugins: [
          Chartist.plugins.tooltip()
        ]
      });

      new Chartist.Line(".chart-area-2", {
        series: [
          [20, 80, 67, 120, 132, 66, 97]
        ]
      }, {
        width: "120px",
        height: "107px",

        showPoint: false,
        showLine: true,
        showArea: true,
        fullWidth: true,
        showLabel: false,
        axisX: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        chartPadding: 0,
        low: 0,
      });

      new Chartist.Line(".chart-area-3", {
        series: [
          [42, 40, 80, 67, 84, 20, 97]
        ]
      }, {
        width: "120px",
        height: "107px",

        showPoint: false,
        showLine: true,
        showArea: true,
        fullWidth: true,
        showLabel: false,
        axisX: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        },
        chartPadding: 0,
        low: 0,
      });

    })
  })(jQuery)
  */
</script>
<!-- END: page scripts -->
<div class="footer">
  <div class="footer__bottom">
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-8">
        <div class="footer__company">
          <!-- <img class="footer__company-logo" src="<?php echo base_url() . "assets/" ?>components/dummy-assets/common/img/footer_logo.png" title="Mediatec Software"> -->
          <span>
            © 2019 <a href="javascript:void(0);" target="_blank">Braincoders Technologies</a>
           
            All rights reserved
          </span>
        </div>
      </div>
    </div>
  </div>
 <!--  <a href="javascript: void(0);" class="utils__scroll-top" onclick="$('body, html').animate({'scrollTop': 0}, 500)"><i
    class="icmn-arrow-up"></i></a> -->
</div>
<script src="<?php echo base_url().'assets' ?>/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- FORM VALIDATION JS -->
<script type="text/javascript" src="<?php echo base_url(). 'assets/jquery-validate/jquery.validate.min.js' ;?>"></script>
<!-- ADDING LOADER JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.js"></script>
<script type="text/javascript" src="<?php echo base_url(). 'assets/js/loader.js' ;?>"></script>
<!-- ADDING LOADER JS -->
<!-- ADDING CUSTOM JS -->
<?php if(isset($addJs) && !empty($addJs)) {
    foreach($addJs as $js) { ?>
        <script src="<?php echo base_url($js); ?>"></script>
    <?php }
} ?>
<script type="text/javascript">
  $(document).ready(function () {
    //
    //To hide the form submit loader
    $('#loading').hide();
    //
    var screenHeight = $(window).height();
    var navHeight = $('.top-bar').outerHeight();
    var footerHeight = $('.footer').outerHeight();
    var displayScreen = screenHeight - navHeight - footerHeight;
    $(".utils__content").css("min-height", displayScreen);
  });
  
</script>
