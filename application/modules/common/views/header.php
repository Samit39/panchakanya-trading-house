<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Panchakanya Trading House Application</title>
    <link href="<?php echo base_url() . "assets/" ?>components/dummy-assets/common/img/icon/favicon.png" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,700i" rel="stylesheet">

    <!-- VENDORS -->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/datatables/media/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/font-linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/font-icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/vendors/font-awesome/css/font-awesome.min.css">
    <!--DataTables Export CSS Starts -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets' ?>/datatablebuttons/css/buttons.dataTables.min.css">
    <!-- Nepali Date Picker -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>vendors/nepali-datepicker/nepali.datepicker.v2.2.min.css">
    <!--DataTables Export CSS Ends-->
    <script src="<?php echo base_url().'assets' ?>/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/popper.js/dist/umd/popper.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
     <script src="<?php echo base_url().'assets' ?>/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <!-- <script src="<?php echo base_url().'assets' ?>/vendors/datatables-responsive/js/dataTables.responsive.js"></script> -->
    
    <script src="<?php echo base_url().'assets' ?>/vendors/jquery-validate/jquery.validate.js"></script>
    <script src="<?php echo base_url().'assets' ?>/vendors/jquery-validate/additional-methods.min.js"></script><!-- Nepali DatePicker JS -->
    <script src="<?php echo base_url() . 'assets/' ?>vendors/nepali-datepicker/nepali.datepicker.v2.2.min.js"></script>
    <!--DataTables Export JS -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> -->
    <script src="<?php echo base_url().'assets' ?>/datatablebuttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/jszip.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets' ?>/datatablebuttons/js/buttons.print.min.js"></script>
    <!--DataTables Export JS -->

    <!-- CLEAN UI HTML ADMIN TEMPLATE MODULES-->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/core/common/core.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/core/widgets/widgets.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/vendors/common/vendors.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/settings/common/settings.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/settings/themes/themes.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/menu-left/common/menu-left.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/menu-right/common/menu-right.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/top-bar/common/top-bar.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/footer/common/footer.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/pages/common/pages.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/ecommerce/common/ecommerce.cleanui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>components/apps/common/apps.cleanui.css">
    <!-- Custom CSS -->
    <script src="<?php echo base_url() . 'assets/' ?>components/menu-left/common/menu-left.cleanui.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>components/menu-right/common/menu-right.cleanui.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>css/form_submit_loader.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>css/loader.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' ?>css/custom.css">
</head>
