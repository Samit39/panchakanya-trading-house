<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Items";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('items_model', 'items');
    }

	public function index()
	{   
		$this->load->library('form_validation');
		$data = array(); 
		$data['breadcrumbs_title'] = "Items Manager";
		$data['content'] = '_list';
		$data['information_list'] = $this->items->getAllInformation();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$item_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->items->getAllUsersList();
		//
		if($item_id!='')
		{
			$data['items_data'] = $this->items->get_detail_by_id($item_id);
			$data['item_id'] = $item_id;
			// pr($data['items_data']);
			// echo $data['items_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->items->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($item_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_items
				$tbl_items_data['title'] = $post['title'];
				$tbl_items_data['description'] = $post['description'];
				$tbl_items_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_items_data['status'] = 1;
				$tbl_items_data['updated_by'] = $user_data['user_id'];
				//
				$this->items->update_information($item_id,$tbl_items_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('items');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_items
				$tbl_items_data['title'] = $post['title'];
				$tbl_items_data['description'] = $post['description'];
				$tbl_items_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_items_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_items_data['status'] = 1;
				$tbl_items_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_items_id = $this->items->insert_new_information($tbl_items_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('items');
			}
			// } //Form Validation	
		}
		$data['breadcrumbs_title'] = "Add Information";
		$data['content'] = '_form';
		// $data['addJs'] = array('assets/js/items.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$items_id = segment(3);
		if($items_id !='')
		{
			$this->items->delete_items($items_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('items');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "items Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->items->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$items_id = segment(3);
		if($items_id !='')
		{
			$this->items->recycle_items($items_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In items.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('items/recycle_bin_list');
	}
}
