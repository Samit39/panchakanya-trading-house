<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stocks extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->permission_name = "Stocks";
        // is this module accessible for this group check_permission();
		//
		if(!$this->check_permission($this->permission_name))
		{
			$this->session->set_flashdata('error', 'Sorry You Dont Have Necessary Permission!');
			redirect('dashboard');
		}
        $this->load->model('stocks_model', 'stocks');
    }

	public function index()
	{   
		$data = array(); 
		$this->load->library('form_validation');
		$this->list_page($data);
	}

	public function search()
	{
		$data['labs_result'] = $this->stocks->getAllActiveLabs();
		$post = $_POST;
		if($post['lab_select'] != '')
		{
			$data['information_list'] = $this->stocks->getAllInformationByLabID($post['lab_select']);
			$data['lab_id'] = $post['lab_select'];
			// print_r($data['information_list']);
			// die();
		}
		$this->list_page($data);
	}

	public function list_page($data)
	{
		$data['breadcrumbs_title'] = "Stocks Manager";
		$data['content'] = '_list';
		// $data['information_list'] = $this->stocks->getAllInformation();
		$data['labs_result'] = $this->stocks->getAllActiveLabs();
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function create()
	{
		$stock_id = segment(3);
		$post = $_POST;
		$user_data = $this->session->all_userdata();
		//
		// $data['users_list'] = $this->stocks->getAllUsersList();
		//
		if($stock_id!='')
		{
			$data['stocks_data'] = $this->stocks->get_detail_by_id($stock_id);
			$data['stock_id'] = $stock_id;
			// pr($data['stocks_data']);
			// echo $data['stocks_data']->name;
			// die();
		}
		if(!empty($post))
		{

			// $this->form_validation->set_rules($this->stocks->rules());


			// if ($this->form_validation->run() == true)
			// {

			if($stock_id!='')
			{
				//EDIT ko lagi
				// Data For tbl_stocks
				$tbl_stocks_data['lab_id'] = $post['lab_select'];
				$tbl_stocks_data['stock_item_code'] = $post['stock_item_code'];
				$tbl_stocks_data['stock_item_name'] = $post['stock_item_name'];
				$tbl_stocks_data['stock_item_count'] = $post['stock_item_count'];
				$tbl_stocks_data['stock_item_description'] = $post['stock_item_description'];
				$tbl_stocks_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_stocks_data['status'] = 1;
				$tbl_stocks_data['updated_by'] = $user_data['user_id'];
				//
				$this->stocks->update_information($stock_id,$tbl_stocks_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Edited In Database.');
				redirect('stocks');
			}
			else
			{
				//Add ko lagi
				// Data For tbl_stocks
				$tbl_stocks_data['lab_id'] = $post['lab_select'];
				$tbl_stocks_data['stock_item_code'] = $post['stock_item_code'];
				$tbl_stocks_data['stock_item_name'] = $post['stock_item_name'];
				$tbl_stocks_data['stock_item_count'] = $post['stock_item_count'];
				$tbl_stocks_data['stock_item_description'] = $post['stock_item_description'];
				$tbl_stocks_data['created_date'] = date("Y-m-d h:i:s");
				$tbl_stocks_data['updated_date'] = date("Y-m-d h:i:s");
				$tbl_stocks_data['status'] = 1;
				$tbl_stocks_data['updated_by'] = $user_data['user_id'];
				//
				$inserted_stocks_id = $this->stocks->insert_new_information($tbl_stocks_data);
				//
				$this->session->set_flashdata('success', 'Information Successfully Added To Database.');
				redirect('stocks');
			}
			// } //Form Validation	
		}
		$data['labs_result'] = $this->stocks->getAllActiveLabs();
		$data['breadcrumbs_title'] = "Add Stock Information";
		$data['content'] = '_form';
		$data['addJs'] = array('assets/js/stocks.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);	
	}

	public function delete()
	{
		$stocks_id = segment(3);
		if($stocks_id !='')
		{
			$this->stocks->delete_stocks($stocks_id);
			$this->session->set_flashdata('success', 'Information Successfully Deleted From Database.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('stocks');
	}

	//Start Of Recycle Bin From Here
	public function recycle_bin_list()
	{
		$data['breadcrumbs_title'] = "stocks Recycle Bin";
		$data['content'] = '_recycle_bin_list';
		$data['information_list'] = $this->stocks->getAllInactiveInformation();
		// pr($data['information_list']);
		// die();
		// $data['addJs'] = array('assets/js/abc.js');
		// $data['addCss'] = array('assets/js/abc.js');
		$this->load->view('common/header');
		$this->load->view('common/body',$data);
	}

	public function recycle()
	{
		$stocks_id = segment(3);
		if($stocks_id !='')
		{
			$this->stocks->recycle_stocks($stocks_id);
			$this->session->set_flashdata('success', 'Information Successfully Recycled And Is Now Available In stocks.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Parameter error while deleting group.');
		}
		redirect('stocks/recycle_bin_list');
	}
}
