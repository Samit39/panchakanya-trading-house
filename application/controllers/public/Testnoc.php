<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Testnoc extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function getStatement_post()
	{
		$validationfieldsmissing = array();
  		//
		$from_date = $this->post('FromDate');
		if ($from_date == '') {
			array_push($validationfieldsmissing, 'FromDate');
		}
		$to_date = $this->post('ToDate');
		if ($to_date == '') {
			array_push($validationfieldsmissing, 'ToDate');
		}
		$depo_code = $this->post('DepoCode');
		if ($depo_code == '') {
			array_push($validationfieldsmissing, 'DepoCode');
		}
		$debit_credit_flag = $this->post('DrCrFlag');
		if ($debit_credit_flag == '') {
			array_push($validationfieldsmissing, 'DrCrFlag');
		}
		//
		if (sizeof($validationfieldsmissing) > 0) 
		{
			$this->response(array(
				'status' => false,
				'message' => 'Required fields missing',
				'missingfields' => $validationfieldsmissing
			));
		}
		else
		{
			$empty_array = array();
			for ($i=0; $i < 20 ; $i++) 
			{ 
				$response_data_array[$i] = array(
					'AccountNumber' => "00000000000".($i+1),
					'AccountName' => "NEPAL OIL CORPORATION LIMITED",
					'Currency' => "NPR",
					'DrAmt' => 0,
					'CrAmt' => 2000 + ($i+1),
					'TranDate' => "2019-02-07T00:00:00",
					'TranParticular' => "124 SAURAV THAKUR 255865 DIESEL",
					'RefNumber' => 9810126548 + ($i+1),
					'Remarks' => "SUMAN SHRESTHA",
					'TranId' => "DC29".($i+1),
					'DepoCode' => "BIRGUNJ_DEPO",
					'DealerCode' => 124 + ($i+1),
					'DealerName' => "SAURAV THAKUR",
					'DealerPAN' => "255865",
					'OilType' => "DIESEL"
				);		
			}
			$this->response(array(
				'status' => true,
				'message' => 'Successfull',
				'resultData' => $response_data_array
			));
		}
	}
	//
	function getAllTransactions_post()
	{
		$validationfieldsmissing = array();
  		//
		$from_date = $this->post('FromDate');
		if ($from_date == '') {
			array_push($validationfieldsmissing, 'FromDate');
		}
		$to_date = $this->post('ToDate');
		if ($to_date == '') {
			array_push($validationfieldsmissing, 'ToDate');
		}
		$depo_code = $this->post('DepoCode');
		if ($depo_code == '') {
			array_push($validationfieldsmissing, 'DepoCode');
		}
		//
		if (sizeof($validationfieldsmissing) > 0) 
		{
			$this->response(array(
				'status' => false,
				'message' => 'Required fields missing',
				'missingfields' => $validationfieldsmissing
			));
		}
		else
		{
			$empty_array = array();
			$return_array = array();
			for ($i=0; $i < 5 ; $i++) 
			{ 
				if($i%2 == 0)
				{
					$credit = 1000;
				}
				else
				{
					$credit = 0;
				}
				$response_data_array[$i] = array(
					'TranID' => "S4324672".($i+1),
					'TranDate' => "2019-03-02T00:00:00",
					'Description' => "TRF TO CALL A/C",
					'Credit' => $credit,
					'Debit' => 1000 + ($i*1000)
				);		
			}
			//
			$return_array['OpeningBalance'] = 20000;
			$return_array['Transactions'] = $response_data_array;
			$this->response(array(
				'status' => true,
				'message' => 'Successfull',
				'resultData' => json_encode($return_array)
			));
		}
	}
	//
	function getCurrentAccountBalance_post()
	{
		$validationfieldsmissing = array();
  		//
		$depo_code = $this->post('DepoCode');
		if ($depo_code == '') {
			array_push($validationfieldsmissing, 'DepoCode');
		}
		//
		if (sizeof($validationfieldsmissing) > 0) 
		{
			$this->response(array(
				'status' => false,
				'message' => 'Required fields missing',
				'missingfields' => $validationfieldsmissing
			));
		}
		else
		{
			$this->response(array(
				'status' => true,
				'message' => 'Successfull',
				'account_balance' => '1000000'
			));
		}
	}
}