$(document).ready(function(){ 
	//
	$('#account_holders_name_select').select2();
	//
	fillAccountHoldersNameSelectBoxAndSelectAlreadySelectedValue();
	//
	$(document).on('change', '#account_type_select', function(){  
		var account_type = $('#account_type_select').find(":selected").val();
		populateAccountHoldersNameSelectBox(account_type,'');
	});
	//
});

function fillAccountHoldersNameSelectBoxAndSelectAlreadySelectedValue()
{
	var account_type = $('#account_type_select').find(":selected").val();
	var account_id = $('#account_id').val();
	populateAccountHoldersNameSelectBox(account_type,account_id);
}

function populateAccountHoldersNameSelectBox(account_type,account_id = '')
{
	var ajaxUrl = $('#baseUrl').val()+'payments/getAccountNamesByAccountType';
	var selected = "";
    if(account_type != "")
    {
    	$.ajax({
	        url: ajaxUrl,
	        data: {
	            account_type:account_type
	        },
	        type: 'post',
	        success: function (data) {
	            data = JSON.parse(data);   
	            // console.log(data);
	            // console.log(JSON.stringify(data));
	            // console.log(data.message);
	            if(data.status == true)
	            {
	            	$('#account_holders_name_select')
					    .find('option')
					    .remove()
					    .end()
					    .append('<option value="">--Select Account Holders Name--</option>')
					    .val('');
	            	$.each(data.result, function(i, field){
	                    // console.log(field.title);
	                    if(field.id == account_id)
	                    {
	                    	selected = "selected";
	                    }
	                    else
	                    {
	                    	selected = '';
	                    }
	                    $('#account_holders_name_select').append('<option value="'+field.id+'"'+ selected +'>'+field.title+'</option>');
	                }); //each ends here
	            }
	            else
	            {
	            	alert(data.message);
	            }
	        },//Success ends here
	        error: function (jqXHR, textStatus, errorThrown) {
	            console.log('Internal error: ' + jqXHR.responseText);
	        }
	    }); //Ajax ends here
    }
}