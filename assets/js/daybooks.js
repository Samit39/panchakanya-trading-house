$(document).ready(function(){ 
	//
	$('#date').nepaliDatePicker({
		npdMonth: true,
		npdYear: true,
		onChange: function(){
			// fetchInputAndPerformInterestCalculation();
		}
	});
	//
	$('#truck_select').select2();
	$('#item_select').select2();
	$('#item_provider_select').select2();
	$('#customer_select').select2();
	//
	$('#item_provider_select_div').hide();
	$('#customer_select_div').hide();
	//
	toggleItemProviderCustomerSelectDiv($('#khata_select').find(":selected").val());
	//
	$(document).on('change', '#khata_select', function(){  
		var khata_type = $('#khata_select').find(":selected").val();
		toggleItemProviderCustomerSelectDiv(khata_type);
	});
	//
	$("#date").keydown(function (event) {
	    event.preventDefault();
	});
});



function toggleItemProviderCustomerSelectDiv(khata_type)
{
	if(khata_type!='')
	{
		if(khata_type=="Kharid Khata")
		{
			$('#item_provider_select_div').show();
			$('#customer_select_div').hide();
		}
		else if(khata_type=="Bikri Khata")
		{
			$('#item_provider_select_div').hide();
			$('#customer_select_div').show();
		}

	}
}
//Overriding Error Messages Starts
jQuery.extend(jQuery.validator.messages, {
    required: "This field is *required. <br/>",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});
 //Overriding error message ends
var validator = $("#daybooks_formsubmit").validate({

	rules: {
		date: "required",
		truck_select: "required",
		item_select: "required",
		khata_select: "required",
		item_provider_select: {
            required: function(element) {
                return $("#khata_select").val() == "Kharid Khata";
            }
        },
        customer_select: {
            required: function(element) {
                return $("#khata_select").val() == "Bikri Khata";
            }
        },
        ch_number: {
            required: function(element) {
                return $("#khata_select").val() == "Kharid Khata";
            }
        },
        price: "required"

	},
	errorElement: 'span',
    errorClass: 'form-error',
    messages: {
        required: "* Required",
    },
    submitHandler: function (form) {
	   $('#submit').attr('disabled','disabled');
	   $("#loading").show();
	   form.submit();
	}
});