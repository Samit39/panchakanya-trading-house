$(document).ready(function() {

    jQuery.extend(jQuery.validator.messages, {
        required: "This field is *Required",
        digits: "Please enter in digits",
        email: "Please enter valid email address",

    });
    $.validator.addMethod("nameregx", function(value, element, regexpr) {
        return regexpr.test(value);
    }, "Please enter a valid name.");

    $.validator.addMethod("regx", function(value, element, regexpr) {
        return regexpr.test(value);
    }, "Please enter a valid phone number.");

    $('#btn-submit').on('click', function(e) {
        var valid_form = $("#users-form").valid();
        if (valid_form) {
            $("#users-form").submit();
        }
    });

    $('#users-form').validate({

        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            },

            firstname: {
                required: true,
                nameregx: /^[A-Za-z._ \-]+$/
            },
            lastname: {
                required: true,
                nameregx: /^[A-Za-z._ \-]+$/
            },
            email: {
                required: true,
                email: true
            },
            confirm_password: {
                required: true,
            },

            phone: {
                required: true,
                // digits: true,
                minlength: 10,
                maxlength: 10,
                regx: /^9+([7-8][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]+)/
            },

        },
        errorElement: 'span',
        errorClass: 'form-error',
        messages: {
            required: "This field is * Required",
            digits: "Please enter in digits",
            email: "Please enter valid email address",
        },
        submitHandler: function (form) {
           $('#submit').attr('disabled','disabled');
           $("#loading").show();
           form.submit();
        }

        // submitHandler: function(form) {
        //     // e.preventDefault();
        //     form.submit();
        //     // var _this = form;
        //     // var formData = _this.serialize();
        //     // alert("here");
        //     // console.log(formData);
        //     // // return false;

        // }
    });
});