$(document).ready(function(){ 
	//
	$('#date').nepaliDatePicker({
		npdMonth: true,
		npdYear: true,
		onChange: function(){
			// fetchInputAndPerformInterestCalculation();
		}
	});
	//
	$('#trans_select').select2();
	$('#account_holders_name_select').select2();
	//
	fillAccountHoldersNameSelectBoxAndSelectAlreadySelectedValue();
	//
	$(document).on('change', '#account_type_select', function(){  
		var account_type = $('#account_type_select').find(":selected").val();
		populateAccountHoldersNameSelectBox(account_type,'');
	});
	//
	$("#date").keydown(function (event) {
	    event.preventDefault();
	});
});

function fillAccountHoldersNameSelectBoxAndSelectAlreadySelectedValue()
{
	var account_type = $('#account_type_select').find(":selected").val();
	var account_id = $('#account_id').val();
	populateAccountHoldersNameSelectBox(account_type,account_id);
}

function populateAccountHoldersNameSelectBox(account_type,account_id = '')
{
	var ajaxUrl = $('#baseUrl').val()+'payments/getAccountNamesByAccountType';
	var selected = "";
    if(account_type != "")
    {
    	$.ajax({
	        url: ajaxUrl,
	        data: {
	            account_type:account_type
	        },
	        type: 'post',
	        success: function (data) {
	            data = JSON.parse(data);   
	            // console.log(data);
	            // console.log(JSON.stringify(data));
	            // console.log(data.message);
	            if(data.status == true)
	            {
	            	$('#account_holders_name_select')
					    .find('option')
					    .remove()
					    .end()
					    .append('<option value="">--Select Account Holders Name--</option>')
					    .val('');
	            	$.each(data.result, function(i, field){
	                    // console.log(field.title);
	                    if(field.id == account_id)
	                    {
	                    	selected = "selected";
	                    }
	                    else
	                    {
	                    	selected = '';
	                    }
	                    $('#account_holders_name_select').append('<option value="'+field.id+'"'+ selected +'>'+field.title+'</option>');
	                }); //each ends here
	            }
	            else
	            {
	            	alert(data.message);
	            }
	        },//Success ends here
	        error: function (jqXHR, textStatus, errorThrown) {
	            console.log('Internal error: ' + jqXHR.responseText);
	        }
	    }); //Ajax ends here
    }
}

//Overriding Error Messages Starts
jQuery.extend(jQuery.validator.messages, {
    required: "This field is *required. <br/>",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});
 //Overriding error message ends
var validator = $("#payments_formsubmit").validate({

	rules: {
		date: "required",
		trans_select: "required",
		account_type_select: "required",
		account_holders_name_select: "required",
		payment_amount: "required"
	},
	errorElement: 'span',
    errorClass: 'form-error',
    messages: {
        required: "* Required",
    },
    submitHandler: function (form) {
	   $('#submit').attr('disabled','disabled');
	   $("#loading").show();
	   form.submit();
	}
});