
$(document).ready(function() {
	/*
	$('#dob').datetimepicker({
	        widgetPositioning: {
	          horizontal: 'left'
	        },
	        icons: {
	          time: "fa fa-clock-o",
	          date: "fa fa-calendar",
	          up: "fa fa-arrow-up",
	          down: "fa fa-arrow-down",
	          previous: 'fa fa-arrow-left',
	          next: 'fa fa-arrow-right'
	        },
	        format: 'LL'
	});

	$('.summernote').summernote({
	        height: 350
	});

	$("#frontdesk_formsubmit").submit(function(){
		var textareaValue = $('#summernote_remarks').summernote('code');
		$('#remarks').val(textareaValue);
	});
	*/
	$("#lab_select").select2();
	//
	$('#practical_date').nepaliDatePicker({
		npdMonth: true,
		npdYear: true,
		onChange: function(){
			var nepali_date = $('#practical_date').val();
			var english_date = BS2AD(nepali_date);
			//
			var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			var a = new Date(english_date);
			var day_name = weekday[a.getDay()];
			//
			$('#practical_day').val(day_name);
			//
		}
	});
});
//Overriding Error Messages Starts
    jQuery.extend(jQuery.validator.messages, {
	    required: "This field is *required. <br/>",
	    remote: "Please fix this field.",
	    email: "Please enter a valid email address.",
	    url: "Please enter a valid URL.",
	    date: "Please enter a valid date.",
	    dateISO: "Please enter a valid date (ISO).",
	    number: "Please enter a valid number.",
	    digits: "Please enter only digits.",
	    creditcard: "Please enter a valid credit card number.",
	    equalTo: "Please enter the same value again.",
	    accept: "Please enter a value with a valid extension.",
	    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
	    minlength: jQuery.validator.format("Please enter at least {0} characters."),
	    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
	    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
	    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
	    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	 //Overriding error message ends
    var validator = $("#practicals_formsubmit").validate({

		rules: {
			lab_select: "required",
			practical_date: "required",
			practical_day: "required",
			practical_name: "required",
			practical_teacher: "required",
			practical_group: "required",
			practical_class: "required",
			practical_section: "required"	
		},
		errorElement: 'span',
        errorClass: 'form-error',
        messages: {
            required: "* Required",
        },
        submitHandler: function (form) {
		   $('#submit').attr('disabled','disabled');
		   $("#loading").show();
		   form.submit();
		}
	});