/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.21 : Database - laboratory_inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('k70jveu9sc0bs0pitduo0bdqk3fcufva','::1',1558794923,'__ci_last_regenerate|i:1558794923;'),('qjjlb9er43a1c2hhv6jb55e7ee37ta7p','::1',1558795327,'__ci_last_regenerate|i:1558795327;'),('03jq6kppm1i614mur5ts1e4gemj1k38q','::1',1558796425,'__ci_last_regenerate|i:1558796197;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:1:\"1\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:12:\"super@tb.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|b:0;success|s:23:\"Users added to database\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('cdj6tai3mk49surq6u0t55r85aqsli2g','::1',1558797010,'__ci_last_regenerate|i:1558797002;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:12:\"super@tb.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:12:\"1,2,3,4,5,6,\";'),('1kchrcaq21ohijci3tbtr0kl0j71ngp5','::1',1558797310,'__ci_last_regenerate|i:1558797310;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:12:\"super@tb.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:12:\"1,2,3,4,5,6,\";'),('prrftjm86jo91encvmstpv3lvn6sehsi','::1',1558797612,'__ci_last_regenerate|i:1558797612;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:12:\"super@tb.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:12:\"1,2,3,4,5,6,\";'),('2j7lgbaqd5lurknl81rnlnjrapfuqlbc','::1',1558799823,'__ci_last_regenerate|i:1558799823;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:12:\"super@tb.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:12:\"1,2,3,4,5,6,\";'),('g0lbgpjgtl9gnnktvt5rbc3m63v3d34p','::1',1558800451,'__ci_last_regenerate|i:1558800162;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:12:\"1,2,3,4,5,6,\";'),('0fsjm9hcmja0n4j2tc12no8jgb8hnbmr','::1',1558800694,'__ci_last_regenerate|i:1558800694;');

/*Table structure for table `tbl_email_template` */

DROP TABLE IF EXISTS `tbl_email_template`;

CREATE TABLE `tbl_email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `adminEmail` varchar(255) DEFAULT NULL,
  `adminSubject` varchar(255) DEFAULT NULL,
  `adminMessage` text CHARACTER SET utf8,
  `userSubject` varchar(255) DEFAULT NULL,
  `userMessage` text,
  `status` enum('Active','InActive') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_email_template` */

insert  into `tbl_email_template`(`id`,`name`,`adminEmail`,`adminSubject`,`adminMessage`,`userSubject`,`userMessage`,`status`) values (1,'reset_password','caseysameet@gmail.com','Request Reset Password','','Request Reset Password','<table style=\"width:600px;margin:0 auto;font-family:Arial\">\n	<thead>\n	</thead>\n	<tbody>\n		<tr>\n			<td style=\"padding:30px;font-size:20px;font-weight:700;color:#fff;background-color:#0591d2;padding-left:30px\">Reset Password</td>\n		</tr>\n		<tr>\n			<td style=\"font-weight:400;color:#777;font-size:14px;background-color:#f7f7f7;padding:30px;line-height:21px\"><p>Greetings {{name}},</p>\n\n<p>We have received your request for password reset on Global Journey Application. </p>\r\n\r\n<p>Your New Password is: {{new_password}} </p> \r\n\r\n<p>Please <a href=\"{{link}}\">click here</a> and login to continue.</p>\n\n<p>Thank you.</p>\n\n<p>Global Journey Application</p>\n\n			</td>\n		</tr>\n	</tbody>\n</table>\n','Active'),(2,'send_cronjob_email','caseysameet@gmail.com','Prasamsa GDS Cronjob','','Prasamsa GDS Cronjob','<table style=\"width:600px;margin:0 auto;font-family:Arial\">\n	<thead>\n	</thead>\n	<tbody>\n		<tr>\n			<td style=\"padding:30px;font-size:20px;font-weight:700;color:#fff;background-color:#f3ae15;padding-left:30px\">Prasamsa GDS Cronjob</td>\n		</tr>\n		<tr>\n			<td style=\"font-weight:400;color:#777;font-size:14px;background-color:#f7f7f7;padding:30px;line-height:21px\">\r\n<p>Greetings,</p>\n\n<p>Cronjob Has Been Successfully Executed On <b><i>Prasamsa GDS</i></b></p>\r\n\r\n<p>Cronjob Detail Is:- <br> {{mail_detail_string}}</p> \r\n\n<p>Thank you.</p>\n\n<p>Prasamsa GDS Application</p>\n\n			</td>\n		</tr>\n	</tbody>\n</table>\n','Active');

/*Table structure for table `tbl_group_permission` */

DROP TABLE IF EXISTS `tbl_group_permission`;

CREATE TABLE `tbl_group_permission` (
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `IDX_E7457D98FE54D947` (`group_id`),
  KEY `IDX_E7457D98FED90CCA` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_group_permission` */

insert  into `tbl_group_permission`(`group_id`,`permission_id`) values (13,1),(13,2),(13,3),(13,4),(13,5),(13,6),(14,5);

/*Table structure for table `tbl_groups` */

DROP TABLE IF EXISTS `tbl_groups`;

CREATE TABLE `tbl_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1425BEEE5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_groups` */

insert  into `tbl_groups`(`id`,`name`,`description`,`active`,`created`) values (13,'Super Admin','The Super Admin',1,'2012-09-25 12:48:20'),(14,'Admin','Admin',1,'2016-03-29 16:09:16');

/*Table structure for table `tbl_logreport` */

DROP TABLE IF EXISTS `tbl_logreport`;

CREATE TABLE `tbl_logreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(255) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `moduleTitle` longtext NOT NULL,
  `action` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_95212275A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_logreport` */

insert  into `tbl_logreport`(`id`,`moduleName`,`moduleId`,`moduleTitle`,`action`,`dateTime`,`ip`,`user_id`) values (1,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:39:04','::1',1),(2,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:41:37','::1',1),(3,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:41:48','::1',1),(4,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:43:05','::1',1),(5,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:35','::1',1),(6,'users',37,'Deleted User',7,'2019-05-25 20:43:46','::1',1),(7,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:46','::1',1),(8,'users',38,'Deleted User',7,'2019-05-25 20:43:54','::1',1),(9,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:54','::1',1),(10,'users',39,'User Created with username - deepika',5,'2019-05-25 20:45:25','::1',1),(11,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:45:25','::1',1),(12,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:49:39','::1',1),(13,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:49:48','::1',1),(14,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:51:23','::1',1),(15,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:51:33','::1',1),(16,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:53:30','::1',1),(17,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:54:55','::1',1),(18,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:55:02','::1',1),(19,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:55:13','::1',1),(20,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:55:33','::1',1),(21,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:56:49','::1',1),(22,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:57:30','::1',1),(23,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:58:42','::1',1),(24,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:59:28','::1',1),(25,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:00:18','::1',1),(26,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:01:00','::1',1),(27,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:01:18','::1',1),(28,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:02:38','::1',1),(29,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:47:42','::1',1),(30,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:49:23','::1',1),(31,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:50:24','::1',1),(32,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:52:08','::1',1),(33,'my_account',1,'Email Address Changed Of - superadmin',14,'2019-05-25 21:52:25','::1',1),(34,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:52:31','::1',1),(35,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:53:45','::1',1),(36,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:53:53','::1',1),(37,'login',39,'Logged In By - deepika',8,'2019-05-25 21:54:05','::1',39),(38,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:54:46','::1',39),(39,'login',39,'Logged Out By - deepika',9,'2019-05-25 21:55:01','::1',39),(40,'login',1,'Logged In By - superadmin',8,'2019-05-25 21:55:45','::1',1),(41,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:56:19','::1',1),(42,'login',1,'Logged In By - superadmin',8,'2019-05-25 21:56:27','::1',1),(43,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:56:34','::1',1);

/*Table structure for table `tbl_permissions` */

DROP TABLE IF EXISTS `tbl_permissions`;

CREATE TABLE `tbl_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_77CE310C5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_permissions` */

insert  into `tbl_permissions`(`id`,`name`,`description`) values (1,'Groups','Groups Informations. Add, Edit & Delete.'),(2,'User','Users Informations. Add, Edit & Delete.'),(3,'Permissions','Permissions Informations. Add, Edit & Delete.'),(4,'Groups Permissions','Groups Permissions Informations, Add, Edit & Delete.'),(5,'My Account','All Information In My Account Including Changing Password'),(6,'Log Report','View Log Reports');

/*Table structure for table `tbl_site_setting` */

DROP TABLE IF EXISTS `tbl_site_setting`;

CREATE TABLE `tbl_site_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `infobox` text COLLATE utf8_unicode_ci,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `nepse_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_site_setting` */

insert  into `tbl_site_setting`(`id`,`site_title`,`site_email`,`site_logo`,`address`,`phone`,`meta_keyword`,`meta_description`,`facebook`,`twitter`,`gplus`,`skype`,`youtube`,`latitude`,`longitude`,`infobox`,`instagram`,`status`,`created_by`,`updated_by`,`created_on`,`updated_on`,`nepse_link`,`api_link`,`video_url`,`video_caption`) values (1,'Prasamsa GDS','caseysameet@gmail.com','assets/image/icon/logo.png','','','Educational Consultancy','Prasamsa Ornaments','','','','','','20','20','','','Active',7,3653,1464588001,1546498168,'','','','');

/*Table structure for table `tbl_smtp_details` */

DROP TABLE IF EXISTS `tbl_smtp_details`;

CREATE TABLE `tbl_smtp_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(45) DEFAULT NULL,
  `port_number` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `encryption` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_smtp_details` */

insert  into `tbl_smtp_details`(`id`,`host_name`,`port_number`,`user_name`,`password`,`encryption`) values (1,'smtp.gmail.com','465','caseysameet@gmail.com','9803317482sp','ssl');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `usercode` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `first_login` tinyint(1) NOT NULL,
  `pwd_change_on` datetime DEFAULT NULL,
  `last_logged` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_52CB3065F85E0677` (`username`),
  KEY `IDX_52CB3065F373DCF` (`groups_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`id`,`groups_id`,`username`,`password`,`firstname`,`middlename`,`lastname`,`usercode`,`address`,`phone`,`email`,`active`,`created`,`deleted`,`first_login`,`pwd_change_on`,`last_logged`) values (1,13,'superadmin','9e708679f8e08b4c29999e5c3c0dd72b','Super','','Admin','SU123','Kalikasthan','','caseysameet@gmail.com',1,'2012-11-08 10:35:22',0,1,'2017-11-11 15:54:44','2019-02-06 17:14:18'),(39,14,'deepika','7b6243abd8cc9127309eb5d31d63b203','Deepika','','Budathoki','3RY4x','Pathari','9805344521','deepika.budathoki@gmail.com',1,'2019-05-25 20:45:25',0,0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
