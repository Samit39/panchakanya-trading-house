/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.21 : Database - panchakanya_trading_house
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bk_tbl_trucks` */

DROP TABLE IF EXISTS `bk_tbl_trucks`;

CREATE TABLE `bk_tbl_trucks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bk_tbl_trucks` */

insert  into `bk_tbl_trucks`(`id`,`title`,`description`,`created_date`,`updated_date`,`updated_by`,`status`) values (1,'5586','Tipper / Shyam Don / 9846556622 / Dhading','2019-06-10 10:36:28','2019-06-10 10:36:49',1,1),(2,'4096','Tipper / Ram Babu / 9846558525 / Nuwakot','2019-06-10 10:38:00','2019-06-10 10:38:00',1,1);

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('ij9dgftg4rmuavtt7q2l4bn909iglq05','::1',1560223862,'__ci_last_regenerate|i:1560223584;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";success|s:44:\"Information Successfully Edited In Database.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('q2ccp1q8fr3crqg5joddcbi1dqjo2aqv','::1',1560223927,'__ci_last_regenerate|i:1560223900;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('j2ebg1kqlevoctju5vvoch79luroagc3','::1',1560224427,'__ci_last_regenerate|i:1560224427;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('tgrm87r43r5lsegonc5div47tu22fvgk','::1',1560225444,'__ci_last_regenerate|i:1560225444;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('t5j4kvv9uofh1ekiqklfei6l8u4rct6n','::1',1560225869,'__ci_last_regenerate|i:1560225869;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('agr4cuiq8gljgtdap0vjl5bcolfkhcp1','::1',1560226234,'__ci_last_regenerate|i:1560226233;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('dgctd0m356hk52me5s90cakpbirvq6bv','::1',1560226581,'__ci_last_regenerate|i:1560226581;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('8735bqt421471pbsjovf7rdg99b6tjaq','::1',1560226996,'__ci_last_regenerate|i:1560226994;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('n918jovfr5dk6rtpc0p225pbe6u4nhbu','::1',1560229673,'__ci_last_regenerate|i:1560229673;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('4un8nav6rvodohc73b0vvulgmttghlup','::1',1560230132,'__ci_last_regenerate|i:1560230036;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('vuselfqmnfac181a9sjcj2d6g7uigmhg','::1',1560230560,'__ci_last_regenerate|i:1560230354;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,13,14,15,16,17,18,19,\";'),('s8av2pjlvg4jbrfdfe90im43bh7pf9tg','::1',1560230973,'__ci_last_regenerate|i:1560230961;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('f72737dnvs3ifun809o9v1cghbbtvg5o','::1',1560231304,'__ci_last_regenerate|i:1560231272;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('7ipnpq95odg988a2nojsvpse2vgudh3m','::1',1560232022,'__ci_last_regenerate|i:1560232022;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('dka0elijbnnjuhngpsoago5qgsmspebl','::1',1560233147,'__ci_last_regenerate|i:1560232936;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('ke0v2cm39ip32v587v90s2gr2t6a3epm','::1',1560250375,'__ci_last_regenerate|i:1560250360;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('5qpptoasmhl2kuqrbrp01888k8ikt1ep','::1',1560250911,'__ci_last_regenerate|i:1560250911;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('f9o1hek2bhghvdop6qtodgm88pahu7da','::1',1560251488,'__ci_last_regenerate|i:1560251488;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('2ueslodi015svi8hchpusl54qvja98tg','::1',1560251846,'__ci_last_regenerate|i:1560251846;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('h4s6nk7mj00h8qemcn66t2n2541jrlgu','::1',1560252374,'__ci_last_regenerate|i:1560252374;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('4fsfmr65rv0j00k3b0ga2homj8h5sm6b','::1',1560252827,'__ci_last_regenerate|i:1560252827;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('dkhspe9shl6iu71a8gegprvftpbnfjkn','::1',1560253264,'__ci_last_regenerate|i:1560253264;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('d27nn5e6ug9iehdc8aut0cejikrbhod0','::1',1560253576,'__ci_last_regenerate|i:1560253576;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('q6qjf3o8tou708i1befs82qcmi7ankvk','::1',1560254156,'__ci_last_regenerate|i:1560254156;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('6ockhpb8h2u35sshifk22a0t3cq3jenm','::1',1560254462,'__ci_last_regenerate|i:1560254462;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('obfe8iu6t1o83fnifkk8nuoi2lna1qkj','::1',1560254766,'__ci_last_regenerate|i:1560254766;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('65irmmaab312539km17egrglbdh2sk27','::1',1560261077,'__ci_last_regenerate|i:1560261077;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('6cec2ddq3qd3hfnm76kde9u6tshup5hs','::1',1560261706,'__ci_last_regenerate|i:1560261632;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('7ma31dq6hs6ap55fa8c6809ugqhv1b3q','::1',1560262573,'__ci_last_regenerate|i:1560262313;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('h99driljllcju9u96g714563a7cdhpkh','::1',1560262622,'__ci_last_regenerate|i:1560262621;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('tq0iufob77khrlnofh1ajn4hebhbgm89','::1',1560263208,'__ci_last_regenerate|i:1560263038;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('73v1jj9d4pf6t4g31pjoqkio4k150foj','::1',1560263387,'__ci_last_regenerate|i:1560263387;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:36:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,\";'),('oovmfnbp7arncnqbaia2347roranttma','::1',1560265042,'__ci_last_regenerate|i:1560265032;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('ct3rvb1ko4lkmu28jh2gurc31qhmvo8a','::1',1560267751,'__ci_last_regenerate|i:1560267751;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('e2rllli6qgk6mj3vs5hhjid6eiu98pe7','::1',1560268257,'__ci_last_regenerate|i:1560268257;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('jcp2pg0p59gqul45ejo5g1s8trd9uddi','::1',1560268661,'__ci_last_regenerate|i:1560268661;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('e9gi9f9qh94nlm7bod2me3vkbkhh6n29','::1',1560269016,'__ci_last_regenerate|i:1560269016;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('pgiv7kc9tot8cqeafn8hv4nlea1uh8jv','::1',1560269374,'__ci_last_regenerate|i:1560269374;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('455kb3t6hhfub3m5g09mhtbsl13op1u3','::1',1560269675,'__ci_last_regenerate|i:1560269675;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('60fa019ohj128rq2jm9nsevjocstmf4m','::1',1560270066,'__ci_last_regenerate|i:1560270066;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('3fc739gkps0okulcgi3gn6ilnluimd8h','::1',1560347506,'__ci_last_regenerate|i:1560347491;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('3h8qd2sh8svn027n7d7chr70if22ds19','::1',1560348257,'__ci_last_regenerate|i:1560348011;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('jc6gmbevp2dlh3h7mqcmpd9cc7fna5m1','::1',1560348514,'__ci_last_regenerate|i:1560348394;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('r7ingl5sdbbo8vfrtm09qj2k01hpfpoj','::1',1560351884,'__ci_last_regenerate|i:1560351599;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('1dmi4vbsnjfug99h56etqshsjqa47fmk','::1',1560352218,'__ci_last_regenerate|i:1560351918;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('om0ug07k6t1ev9vvk2b7iepvil82126j','::1',1560352380,'__ci_last_regenerate|i:1560352297;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('sdku9ckhopg4t5epdn32rcjs4nk64ghn','::1',1560353237,'__ci_last_regenerate|i:1560353237;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('bjhaui18gcgc1ls0c6nev1rceopbsd79','::1',1560353832,'__ci_last_regenerate|i:1560353832;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('79qnuqo73j3oh7jmong47qclfuiuvd8a','::1',1560354206,'__ci_last_regenerate|i:1560354206;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('93p67kceh2flstshvj3guuerg8qor4fo','::1',1560355178,'__ci_last_regenerate|i:1560355178;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('lco04mt1nuc53redci95ptevgj62j0j5','::1',1560355761,'__ci_last_regenerate|i:1560355761;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('iviej5efameuqrahb3jqebca5rs9gnme','::1',1560356095,'__ci_last_regenerate|i:1560356095;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('66r0cbskhvh9jta7dakglr7hggkcuof0','::1',1560356416,'__ci_last_regenerate|i:1560356416;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('iva031i525ultes0t87mmmtlq5jfqrr8','::1',1560356762,'__ci_last_regenerate|i:1560356762;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('u6eui5p1fkb5ihu7eem247pjkr0d93pi','::1',1560357089,'__ci_last_regenerate|i:1560357089;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('e8ruir06q3r5d8tp9du65knjum44co9m','::1',1560357452,'__ci_last_regenerate|i:1560357452;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('qd5egil7qs1a968vucmi82hu8hm2aofd','::1',1560357837,'__ci_last_regenerate|i:1560357837;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('sojhcinio5rra2pgphl6l8ral9mib3cd','::1',1560358237,'__ci_last_regenerate|i:1560358237;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('uva8nudiljranj0f87enq0cc4ntnfr3k','::1',1560358545,'__ci_last_regenerate|i:1560358545;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('nfq4etl5j7f3g9etk32q88ktmtrj2u8r','::1',1560617585,'__ci_last_regenerate|i:1560617571;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('kbv240rkgn0c0qbrnbp9ho0d3d4dtml3','::1',1560618156,'__ci_last_regenerate|i:1560617886;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('0hpuisp82ufeiirjc3bohi3cimh4insh','::1',1560618255,'__ci_last_regenerate|i:1560618255;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('j4frk2vpj22jfo9ivtocj6ekf3nrq3ri','::1',1560618589,'__ci_last_regenerate|i:1560618589;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('crlku7bl2t2hijkcbkgp30h57r92vj2l','::1',1560619161,'__ci_last_regenerate|i:1560619161;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('omcmcnukbl4onp32egbo050ojuunqo00','::1',1560619730,'__ci_last_regenerate|i:1560619467;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('im3gm48hq257ar83i2tqvi98pm27k4k9','::1',1560619837,'__ci_last_regenerate|i:1560619837;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('7mtth9f4p1a569rds4dcgcaiq0a93jjl','::1',1560620438,'__ci_last_regenerate|i:1560620164;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('t4a1nhgdh01v3ahm599po165qaosri7c','::1',1560620520,'__ci_last_regenerate|i:1560620520;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('h7ltg7187slbkjqkssuc8qlglgu7f525','::1',1560620829,'__ci_last_regenerate|i:1560620829;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('91fi1663833dauufdamka54e9s3mk93i','::1',1560621405,'__ci_last_regenerate|i:1560621177;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('5rv0i0n4k9qgmh5lg5ql6uq4g6savo7n','::1',1560621818,'__ci_last_regenerate|i:1560621527;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('npgopaukto0n9tdjc00p8smcn3c9iohk','::1',1560622134,'__ci_last_regenerate|i:1560621851;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:21:\"caseysameet@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:39:\"1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,\";'),('68jbtidljr6ekbka7vd5bk9emisfn45s','::1',1560622203,'__ci_last_regenerate|i:1560622194;is_logged_in|b:1;user_id|s:2:\"40\";group_id|s:2:\"14\";username|s:6:\"prabin\";full_name|s:12:\"Prabin  Puri\";email|s:16:\"prabin@gmail.com\";user_created_date|s:19:\"2019-06-15 23:54:47\";accessible_module|s:23:\"5,15,16,17,18,19,20,21,\";'),('mmr8fjjabtifu2gtm3v8fs76d0h62uc4','::1',1560622971,'__ci_last_regenerate|i:1560622961;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('53fkmaao7lj924uqc4rntbo3267ha47g','::1',1560699471,'__ci_last_regenerate|i:1560699381;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('cmmqlme8g6jn8aai804v82akckqeq66f','::1',1560699708,'__ci_last_regenerate|i:1560699684;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('aiat962okgnlhhih0luubkgtgsj2fq1r','::1',1560700219,'__ci_last_regenerate|i:1560700197;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('52ehpf5nc3v9f07cinadgbslk6o0qkcb','::1',1560700569,'__ci_last_regenerate|i:1560700569;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('pdjddmc74sj3p0o4hsbus3m4sutg31da','::1',1560701384,'__ci_last_regenerate|i:1560701384;'),('jjct8pi3v9k695ruqojvbu7d50tv0c6t','::1',1561548289,'__ci_last_regenerate|i:1561548273;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('qkc05tcmu17kd4gs3noq3v2e2bg4q2ft','::1',1561549618,'__ci_last_regenerate|i:1561549618;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('69a0actnom2pnhl3cff23u8g0d4v126e','::1',1561567762,'__ci_last_regenerate|i:1561567476;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";success|s:44:\"Information Successfully Edited In Database.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('8idvu2sgeuabp1v57kbkags6bjkua8if','::1',1561568020,'__ci_last_regenerate|i:1561567790;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";'),('r62gdt6c4jb4i73ieh2a7onln40irpro','::1',1561568362,'__ci_last_regenerate|i:1561568104;is_logged_in|b:1;user_id|s:1:\"1\";group_id|s:2:\"13\";username|s:10:\"superadmin\";full_name|s:12:\"Super  Admin\";email|s:24:\"caseysameet002@gmail.com\";user_created_date|s:19:\"2012-11-08 10:35:22\";accessible_module|s:33:\"1,2,3,4,5,6,15,16,17,18,19,20,21,\";success|s:43:\"Information Successfully Added To Database.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('j8b6lknsucrbjorh8dduerse4bj5cgll','::1',1561568625,'__ci_last_regenerate|i:1561568617;is_logged_in|b:1;user_id|s:2:\"41\";group_id|s:2:\"14\";username|s:4:\"anil\";full_name|s:10:\"Anil  Puri\";email|s:18:\"anilpuri@gmail.com\";user_created_date|s:19:\"2019-06-26 22:48:29\";accessible_module|s:20:\"5,15,16,17,19,20,21,\";');

/*Table structure for table `tbl_customers` */

DROP TABLE IF EXISTS `tbl_customers`;

CREATE TABLE `tbl_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_customers` */

/*Table structure for table `tbl_daybooks` */

DROP TABLE IF EXISTS `tbl_daybooks`;

CREATE TABLE `tbl_daybooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_provider_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `ch_number` varchar(255) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_daybooks` */

/*Table structure for table `tbl_email_template` */

DROP TABLE IF EXISTS `tbl_email_template`;

CREATE TABLE `tbl_email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `adminEmail` varchar(255) DEFAULT NULL,
  `adminSubject` varchar(255) DEFAULT NULL,
  `adminMessage` text CHARACTER SET utf8,
  `userSubject` varchar(255) DEFAULT NULL,
  `userMessage` text,
  `status` enum('Active','InActive') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_email_template` */

insert  into `tbl_email_template`(`id`,`name`,`adminEmail`,`adminSubject`,`adminMessage`,`userSubject`,`userMessage`,`status`) values (1,'reset_password','caseysameet@gmail.com','Request Reset Password','','Request Reset Password','<table style=\"width:600px;margin:0 auto;font-family:Arial\">\n	<thead>\n	</thead>\n	<tbody>\n		<tr>\n			<td style=\"padding:30px;font-size:20px;font-weight:700;color:#fff;background-color:#0591d2;padding-left:30px\">Reset Password</td>\n		</tr>\n		<tr>\n			<td style=\"font-weight:400;color:#777;font-size:14px;background-color:#f7f7f7;padding:30px;line-height:21px\"><p>Greetings {{name}},</p>\n\n<p>We have received your request for password reset on Laboratory Management Application.</p>\r\n\r\n<p>Your New Password is: {{new_password}} </p> \r\n\r\n<p>Please <a href=\"{{link}}\">click here</a> and login to continue.</p>\n\n<p>Thank you.</p>\n\n<p>Laboratory Management Application</p>\n\n			</td>\n		</tr>\n	</tbody>\n</table>\n','Active');

/*Table structure for table `tbl_group_permission` */

DROP TABLE IF EXISTS `tbl_group_permission`;

CREATE TABLE `tbl_group_permission` (
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `IDX_E7457D98FE54D947` (`group_id`),
  KEY `IDX_E7457D98FED90CCA` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_group_permission` */

insert  into `tbl_group_permission`(`group_id`,`permission_id`) values (13,1),(13,2),(13,3),(13,4),(13,5),(13,6),(13,15),(13,16),(13,17),(13,19),(13,20),(13,21),(14,5),(14,15),(14,16),(14,17),(14,19),(14,20),(14,21);

/*Table structure for table `tbl_groups` */

DROP TABLE IF EXISTS `tbl_groups`;

CREATE TABLE `tbl_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1425BEEE5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_groups` */

insert  into `tbl_groups`(`id`,`name`,`description`,`active`,`created`) values (13,'Super Admin','The Super Admin',1,'2012-09-25 12:48:20'),(14,'Admin','Admin',1,'2016-03-29 16:09:16');

/*Table structure for table `tbl_items` */

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_items` */

insert  into `tbl_items`(`id`,`title`,`description`,`created_date`,`updated_date`,`updated_by`,`status`) values (1,'MOON Thread','Moon','2019-06-26 10:46:14','2019-06-26 10:46:14',1,1),(2,'Ameto','Coats Ameto','2019-06-26 10:46:38','2019-06-26 10:46:38',1,1);

/*Table structure for table `tbl_logreport` */

DROP TABLE IF EXISTS `tbl_logreport`;

CREATE TABLE `tbl_logreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(255) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `moduleTitle` longtext NOT NULL,
  `action` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_95212275A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_logreport` */

insert  into `tbl_logreport`(`id`,`moduleName`,`moduleId`,`moduleTitle`,`action`,`dateTime`,`ip`,`user_id`) values (1,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:39:04','::1',1),(2,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:41:37','::1',1),(3,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:41:48','::1',1),(4,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:43:05','::1',1),(5,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:35','::1',1),(6,'users',37,'Deleted User',7,'2019-05-25 20:43:46','::1',1),(7,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:46','::1',1),(8,'users',38,'Deleted User',7,'2019-05-25 20:43:54','::1',1),(9,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:43:54','::1',1),(10,'users',39,'User Created with username - deepika',5,'2019-05-25 20:45:25','::1',1),(11,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:45:25','::1',1),(12,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:49:39','::1',1),(13,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:49:48','::1',1),(14,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:51:23','::1',1),(15,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:51:33','::1',1),(16,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:53:30','::1',1),(17,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:54:55','::1',1),(18,'login',1,'Logged Out By - superadmin',9,'2019-05-25 20:55:02','::1',1),(19,'login',1,'Logged In By - superadmin',8,'2019-05-25 20:55:13','::1',1),(20,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:55:33','::1',1),(21,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:56:49','::1',1),(22,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:57:30','::1',1),(23,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:58:42','::1',1),(24,'users',1,'Listing Page Viewed On users',1,'2019-05-25 20:59:28','::1',1),(25,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:00:18','::1',1),(26,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:01:00','::1',1),(27,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:01:18','::1',1),(28,'groups',1,'Listing Page Viewed On groups',1,'2019-05-25 21:02:38','::1',1),(29,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:47:42','::1',1),(30,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:49:23','::1',1),(31,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:50:24','::1',1),(32,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:52:08','::1',1),(33,'my_account',1,'Email Address Changed Of - superadmin',14,'2019-05-25 21:52:25','::1',1),(34,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:52:31','::1',1),(35,'users',1,'Listing Page Viewed On users',1,'2019-05-25 21:53:45','::1',1),(36,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:53:53','::1',1),(37,'login',39,'Logged In By - deepika',8,'2019-05-25 21:54:05','::1',39),(38,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-25 21:54:46','::1',39),(39,'login',39,'Logged Out By - deepika',9,'2019-05-25 21:55:01','::1',39),(40,'login',1,'Logged In By - superadmin',8,'2019-05-25 21:55:45','::1',1),(41,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:56:19','::1',1),(42,'login',1,'Logged In By - superadmin',8,'2019-05-25 21:56:27','::1',1),(43,'login',1,'Logged Out By - superadmin',9,'2019-05-25 21:56:34','::1',1),(44,'login',1,'Tried To Login With Invalid Credential With Username - ',15,'2019-05-26 10:10:29','::1',NULL),(45,'login',1,'Logged In By - superadmin',8,'2019-05-26 10:10:45','::1',1),(46,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:10:55','::1',1),(47,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:11:47','::1',1),(48,'groups',15,'Created Group - Tet',5,'2019-05-26 10:12:41','::1',1),(49,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:12:41','::1',1),(50,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:13:04','::1',1),(51,'groups',15,'Edited Group - Tetsd',6,'2019-05-26 10:14:12','::1',1),(52,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:14:12','::1',1),(53,'groups',15,'Deleted Group',7,'2019-05-26 10:14:29','::1',1),(54,'groups',1,'Listing Page Viewed On groups',1,'2019-05-26 10:14:29','::1',1),(55,'log_report',1,'Listing Page Viewed On log_report',1,'2019-05-26 10:14:55','::1',1),(56,'login',1,'Logged Out By - superadmin',9,'2019-05-26 10:16:00','::1',1),(57,'login',1,'Logged In By - superadmin',8,'2019-05-26 10:20:25','::1',1),(58,'login',1,'Logged In By - superadmin',8,'2019-05-26 17:20:34','::1',1),(59,'login',1,'Logged Out By - superadmin',9,'2019-05-26 17:45:25','::1',1),(60,'login',1,'Logged In By - superadmin',8,'2019-05-26 17:45:57','::1',1),(61,'login',1,'Logged In By - superadmin',8,'2019-05-26 19:47:59','::1',1),(62,'login',1,'Logged Out By - superadmin',9,'2019-05-26 19:54:19','::1',1),(63,'login',1,'Logged In By - superadmin',8,'2019-05-26 19:54:29','::1',1),(64,'login',1,'Logged Out By - superadmin',9,'2019-05-26 20:03:29','::1',1),(65,'login',1,'Logged In By - superadmin',8,'2019-05-26 20:03:38','::1',1),(66,'login',1,'Logged Out By - superadmin',9,'2019-05-26 21:23:08','::1',1),(67,'login',1,'Logged In By - superadmin',8,'2019-05-26 21:23:16','::1',1),(68,'login',1,'Logged In By - superadmin',8,'2019-05-27 07:51:00','::1',1),(69,'log_report',1,'Listing Page Viewed On log_report',1,'2019-05-27 08:45:42','::1',1),(70,'login',1,'Logged Out By - superadmin',9,'2019-05-27 08:58:41','::1',1),(71,'login',1,'Logged In By - superadmin',8,'2019-05-27 08:58:50','::1',1),(72,'login',1,'Logged Out By - superadmin',9,'2019-05-27 08:59:41','::1',1),(73,'login',1,'Tried To Login With Invalid Credential With Username - superadmin',15,'2019-05-27 08:59:49','::1',NULL),(74,'login',1,'Tried To Login With Invalid Credential With Username - superadmin',15,'2019-05-27 08:59:59','::1',NULL),(75,'login',1,'Logged In By - superadmin',8,'2019-05-27 09:00:08','::1',1),(76,'login',1,'Logged In By - superadmin',8,'2019-05-27 20:51:42','::1',1),(77,'login',1,'Logged Out By - superadmin',9,'2019-05-27 21:49:48','::1',1),(78,'login',39,'Logged In By - deepika',8,'2019-05-27 21:50:39','::1',39),(79,'my_account',1,'Listing Page Viewed On my_account',1,'2019-05-27 22:03:11','::1',39),(80,'login',39,'Logged Out By - deepika',9,'2019-05-27 22:03:20','::1',39),(81,'login',1,'Logged In By - superadmin',8,'2019-05-27 22:05:40','::1',1),(82,'users',1,'Listing Page Viewed On users',1,'2019-05-27 22:05:49','::1',1),(83,'users',40,'User Created with username - test_user',5,'2019-05-27 22:07:24','::1',1),(84,'users',1,'Listing Page Viewed On users',1,'2019-05-27 22:07:24','::1',1),(85,'login',1,'Logged Out By - superadmin',9,'2019-05-27 22:07:43','::1',1),(86,'login',1,'Password Reset From Login Form With Email - caseysameet@gmail.com',16,'2019-05-27 22:07:50','::1',NULL),(87,'login',1,'Password Reset From Login Form With Email - caseysameet@gmail.com',16,'2019-05-27 22:12:20','::1',NULL),(88,'login',1,'Password Reset From Login Form With Email - caseysameet@gmail.com',16,'2019-05-27 22:16:45','::1',NULL),(89,'login',1,'Logged In By - superadmin',8,'2019-05-27 22:17:55','::1',1),(90,'users',1,'Listing Page Viewed On users',1,'2019-05-27 22:18:04','::1',1),(91,'users',40,'Deleted User',7,'2019-05-27 22:18:13','::1',1),(92,'users',1,'Listing Page Viewed On users',1,'2019-05-27 22:18:13','::1',1),(93,'login',1,'Logged Out By - superadmin',9,'2019-05-27 22:43:54','::1',1),(94,'login',39,'Logged In By - deepika',8,'2019-05-27 22:44:09','::1',39),(95,'login',1,'Logged In By - superadmin',8,'2019-06-10 21:24:23','::1',1),(96,'login',1,'Logged Out By - superadmin',9,'2019-06-10 21:44:37','::1',1),(97,'login',1,'Logged In By - superadmin',8,'2019-06-10 21:44:44','::1',1),(98,'login',1,'Logged Out By - superadmin',9,'2019-06-10 22:03:53','::1',1),(99,'login',1,'Logged In By - superadmin',8,'2019-06-10 22:04:01','::1',1),(100,'login',1,'Logged Out By - superadmin',9,'2019-06-10 22:17:25','::1',1),(101,'login',1,'Logged In By - superadmin',8,'2019-06-10 22:17:32','::1',1),(102,'login',1,'Logged Out By - superadmin',9,'2019-06-10 22:31:36','::1',1),(103,'login',1,'Logged In By - superadmin',8,'2019-06-10 22:31:44','::1',1),(104,'login',1,'Logged Out By - superadmin',9,'2019-06-10 22:39:51','::1',1),(105,'login',1,'Logged In By - superadmin',8,'2019-06-11 09:07:24','::1',1),(106,'login',1,'Logged Out By - superadmin',9,'2019-06-11 09:11:24','::1',1),(107,'login',1,'Logged In By - superadmin',8,'2019-06-11 09:11:31','::1',1),(108,'login',1,'Logged Out By - superadmin',9,'2019-06-11 11:14:20','::1',1),(109,'login',1,'Logged In By - superadmin',8,'2019-06-11 11:14:33','::1',1),(110,'login',1,'Tried To Login With Invalid Credential With Username - superadmin',15,'2019-06-11 16:37:46','::1',NULL),(111,'login',1,'Logged In By - superadmin',8,'2019-06-11 16:37:55','::1',1),(112,'login',1,'Logged In By - superadmin',8,'2019-06-11 19:36:17','::1',1),(113,'login',1,'Logged Out By - superadmin',9,'2019-06-11 20:42:12','::1',1),(114,'login',1,'Logged In By - superadmin',8,'2019-06-11 20:42:22','::1',1),(115,'login',1,'Logged In By - superadmin',8,'2019-06-12 19:36:46','::1',1),(116,'login',1,'Logged In By - superadmin',8,'2019-06-15 22:38:05','::1',1),(117,'my_account',1,'Listing Page Viewed On my_account',1,'2019-06-15 23:13:50','::1',1),(118,'users',1,'Listing Page Viewed On users',1,'2019-06-15 23:53:38','::1',1),(119,'users',39,'Deleted User',7,'2019-06-15 23:53:48','::1',1),(120,'users',1,'Listing Page Viewed On users',1,'2019-06-15 23:53:49','::1',1),(121,'users',40,'User Created with username - prabin',5,'2019-06-15 23:54:47','::1',1),(122,'users',1,'Listing Page Viewed On users',1,'2019-06-15 23:54:47','::1',1),(123,'login',1,'Logged Out By - superadmin',9,'2019-06-15 23:54:54','::1',1),(124,'login',40,'Logged In By - prabin',8,'2019-06-15 23:55:06','::1',40),(125,'login',40,'Logged Out By - prabin',9,'2019-06-16 00:03:07','::1',40),(126,'login',1,'Password Reset From Login Form With Email - caseysameet@gmail.com',16,'2019-06-16 00:03:22','::1',NULL),(127,'login',1,'Tried To Login With Invalid Credential With Username - prabin',15,'2019-06-16 00:04:12','::1',NULL),(128,'login',40,'Logged In By - prabin',8,'2019-06-16 00:04:26','::1',40),(129,'my_account',1,'Listing Page Viewed On my_account',1,'2019-06-16 00:04:29','::1',40),(130,'my_account',40,'Password Changed Of - prabin',14,'2019-06-16 00:04:44','::1',40),(131,'login',40,'Logged Out By - prabin',9,'2019-06-16 00:04:48','::1',40),(132,'login',40,'Logged In By - prabin',8,'2019-06-16 00:05:01','::1',40),(133,'login',40,'Logged Out By - prabin',9,'2019-06-16 00:07:41','::1',40),(134,'login',1,'Logged In By - superadmin',8,'2019-06-16 00:07:51','::1',1),(135,'login',1,'Logged In By - superadmin',8,'2019-06-16 21:22:51','::1',1),(136,'login',1,'Logged Out By - superadmin',9,'2019-06-16 21:54:44','::1',1),(137,'login',1,'Logged In By - superadmin',8,'2019-06-26 17:09:49','::1',1),(138,'login',1,'Tried To Login With Invalid Credential With Username - superadmin',15,'2019-06-26 22:29:42','::1',NULL),(139,'login',1,'Logged In By - superadmin',8,'2019-06-26 22:29:52','::1',1),(140,'users',1,'Listing Page Viewed On users',1,'2019-06-26 22:47:34','::1',1),(141,'users',40,'Deleted User',7,'2019-06-26 22:47:39','::1',1),(142,'users',1,'Listing Page Viewed On users',1,'2019-06-26 22:47:40','::1',1),(143,'users',41,'User Created with username - anil',5,'2019-06-26 22:48:29','::1',1),(144,'users',1,'Listing Page Viewed On users',1,'2019-06-26 22:48:30','::1',1),(145,'login',1,'Logged Out By - superadmin',9,'2019-06-26 22:48:37','::1',1),(146,'login',41,'Logged In By - anil',8,'2019-06-26 22:48:47','::1',41);

/*Table structure for table `tbl_payments` */

DROP TABLE IF EXISTS `tbl_payments`;

CREATE TABLE `tbl_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) DEFAULT NULL,
  `trans_type` enum('Dr','Cr') DEFAULT NULL,
  `account_type` enum('Item Providers','Customers') DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `payment_amount` double(10,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_payments` */

/*Table structure for table `tbl_permissions` */

DROP TABLE IF EXISTS `tbl_permissions`;

CREATE TABLE `tbl_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_77CE310C5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_permissions` */

insert  into `tbl_permissions`(`id`,`name`,`description`) values (1,'Groups','Groups Informations. Add, Edit & Delete.'),(2,'User','Users Informations. Add, Edit & Delete.'),(3,'Permissions','Permissions Informations. Add, Edit & Delete.'),(4,'Groups Permissions','Groups Permissions Informations, Add, Edit & Delete.'),(5,'My Account','All Information In My Account Including Changing Password'),(6,'Log Report','View Log Reports'),(15,'Items','Add, Edit, Delete Items'),(16,'Providers','Item Providers Add, Edit, Delete'),(17,'Customers','Add, Edit And Delete Customers'),(19,'Daybooks','Add, Edit, Delete Daybook Record'),(20,'Payments','Manage all payments'),(21,'Statements','Statement Add Edit Delete');

/*Table structure for table `tbl_providers` */

DROP TABLE IF EXISTS `tbl_providers`;

CREATE TABLE `tbl_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_providers` */

/*Table structure for table `tbl_site_setting` */

DROP TABLE IF EXISTS `tbl_site_setting`;

CREATE TABLE `tbl_site_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `infobox` text COLLATE utf8_unicode_ci,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `nepse_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_send_type` enum('SWIFTSEND','PHPNATIVEMAIL') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_site_setting` */

insert  into `tbl_site_setting`(`id`,`site_title`,`site_email`,`site_logo`,`address`,`phone`,`meta_keyword`,`meta_description`,`facebook`,`twitter`,`gplus`,`skype`,`youtube`,`latitude`,`longitude`,`infobox`,`instagram`,`status`,`created_by`,`updated_by`,`created_on`,`updated_on`,`nepse_link`,`api_link`,`video_url`,`video_caption`,`mail_send_type`) values (1,'Prasamsa GDS','caseysameet@gmail.com','assets/image/icon/logo.png','','','Educational Consultancy','Prasamsa Ornaments','','','','','','20','20','','','Active',7,3653,1464588001,1546498168,'','','','','SWIFTSEND');

/*Table structure for table `tbl_smtp_details` */

DROP TABLE IF EXISTS `tbl_smtp_details`;

CREATE TABLE `tbl_smtp_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(45) DEFAULT NULL,
  `port_number` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `encryption` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_smtp_details` */

insert  into `tbl_smtp_details`(`id`,`host_name`,`port_number`,`user_name`,`password`,`encryption`) values (1,'mail.logicaletter.com','465','samit.puri@logicaletter.com','9803317482Sp','ssl');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `usercode` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `first_login` tinyint(1) NOT NULL,
  `pwd_change_on` datetime DEFAULT NULL,
  `last_logged` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_52CB3065F85E0677` (`username`),
  KEY `IDX_52CB3065F373DCF` (`groups_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`id`,`groups_id`,`username`,`password`,`firstname`,`middlename`,`lastname`,`usercode`,`address`,`phone`,`email`,`active`,`created`,`deleted`,`first_login`,`pwd_change_on`,`last_logged`) values (1,13,'superadmin','9e708679f8e08b4c29999e5c3c0dd72b','Super','','Admin','SU123','Kalikasthan','','caseysameet002@gmail.com',1,'2012-11-08 10:35:22',0,1,'2017-11-11 15:54:44','2019-02-06 17:14:18'),(41,14,'anil','a2af486ce7125f6a0ee4524f435cb5e8','Anil','','Puri','Wlc3a','Nagarjun - 06, Dandapauwa','9843090333','anilpuri@gmail.com',1,'2019-06-26 22:48:29',0,0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
